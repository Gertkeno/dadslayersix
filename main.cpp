#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__
#include <time.h>
#include <random>

#include <mechanical/GameManager.h>
#include <mechanical/TexHolder.h>
#include <gert_Wrappers.h>
#include <gert_FontWrapper.h>
#include <gert_SoundWrapper.h>

#include <mechanical/Menu.h>

SDL_Rect gCamera;
SDL_Renderer* gRenderer;
double gFrameTime;

GameManager* gGameManager;

MultiFrame_Wrap* gTextures;
Sound_Wrapper* gSounds;
Font_Wrapper* gFont;

void load_tex( void )
{
    using namespace TexHolder;
    gTextures = new MultiFrame_Wrap[ tTOTAL ];
    gTextures[ tCIRCLE ].load_texture( "assets/Circle.png" );
    gTextures[ tBULLET ].load_texture( "assets/Bullet.png" );
    gTextures[ tCHEST ].load_texture( "assets/chect.png" );
    gTextures[ tDAD_GRUNT ].load_texture( "assets/dads/gruntDOWN.png" );
    gTextures[ tWIRE ].load_texture( "assets/dads/wire.png" );
    gTextures[ tDAD_ENGI ].load_texture( "assets/dads/engiDOWN.png" );
    gTextures[ tBLOOD ].load_texture( "assets/bloodss.png", "xml" );
    gTextures[ tGIB ].load_texture( "assets/gibs.png", "xml" );
    gTextures[ tRING ].load_texture( "assets/ring.png" );
    gTextures[ tTURRET ].load_texture( "assets/turret.png", 25, 1, 100, 100 );
    gTextures[ tBACKGROUND ].load_texture( "assets/background.png" );

    gTextures[ tDOM_BACK ].load_texture( "assets/domback.png", 19, 1, 40, 60 );
    gTextures[ tDOM_FRONT ].load_texture( "assets/domfront.png", 19, 1, 40, 60 );
    gTextures[ tDOM_STANDING ].load_texture( "assets/domstand.png" );
    gTextures[ tDOM_RIGHT ].load_texture( "assets/domright.png", 19, 1, 40, 60 );

    gTextures[ tGIGA_HAND ].load_texture( "assets/dads/gigadadhand.png", 25, 1, 80, 120 );
    gTextures[ tGIGA_HEAD ].load_texture( "assets/dads/gigadadhead.png" );

    gFont = new Font_Wrapper( "assets/pricedown_bl.ttf", 28 );

    gSounds = new Sound_Wrapper[ sTOTAL ];
    gSounds[ s0DAD_SPAWN ].load_sound( "assets/sound/catch.wav" );
    gSounds[ s1DAD_SPAWN ].load_sound( "assets/sound/golong.wav" );
    gSounds[ s2DAD_SPAWN ].load_sound( "assets/sound/heycamper.wav" );
    gSounds[ s3DAD_SPAWN ].load_sound( "assets/sound/heysport.wav" );
    gSounds[ s4DAD_SPAWN ].load_sound( "assets/sound/hugme.wav" );

    gSounds[ sPLAYERDEATH ].load_sound( "assets/sound/death.wav" );
    gSounds[ sPLAYERDEATH ].set_volume( MIX_MAX_VOLUME / 2.0 );

    gSounds[ s0DAD_HIT ].load_sound( "assets/sound/scream.wav" );

    gSounds[ s0DAD_SCREAM ].load_sound( "assets/sound/scream0.wav" );
    gSounds[ s1DAD_SCREAM ].load_sound( "assets/sound/scream1.wav" );
    gSounds[ s2DAD_SCREAM ].load_sound( "assets/sound/scream2.wav" );

    gSounds[ sPEW ].load_sound( "assets/sound/pew.wav" );
    gSounds[ sPEW ].set_volume( MIX_MAX_VOLUME / 20.0 );
}

void close_tex( void )
{
    delete[] gTextures;

    delete gFont;

    delete[] gSounds;
}

bool load_options( void )
{
    bool fscreen = false;
    SDL_RWops* settingFile = SDL_RWFromFile( "settings.ds6", "r" );
    if( settingFile == NULL )
    {
        #ifdef __GERT_DEBUG__
        std::cout << "Couldn't load settings.ds6" << std::endl;
        #endif // __GERT_DEBUG__
        return false;
    }
    #define EASYREAD( p, f ) SDL_RWread( settingFile, &p, sizeof( f ), 1 );

    EASYREAD( fscreen, bool )
    EASYREAD( gCamera.w, int )
    EASYREAD( gCamera.h, int )
    EASYREAD( Menu::percentVolume, float )
    EASYREAD( Menu::percentMusic, float )

    SDL_RWclose( settingFile );

    Mix_Volume( -1, MIX_MAX_VOLUME * Menu::percentVolume );
    Mix_VolumeMusic( MIX_MAX_VOLUME * Menu::percentMusic );
    return fscreen;
}

int main( int argc, char* argv[] )
{
    #ifdef __GERT_DEBUG__
    std::cout << "Seeding rand...\n";
    #endif // __GERT_DEBUG__
    srand( time( 0 ) );

    #ifdef __GERT_DEBUG__
    std::cout << "Initalizing SDL...\n";
    #endif // __GERT_DEBUG__
    if( SDL_Init( SDL_INIT_EVERYTHING ) < 0 )
    {
        #ifdef __GERT_DEBUG__
        std::cout << SDL_GetError() << std::endl;
        #endif // __GERT_DEBUG__
        return 1;
    }
    if( IMG_Init( IMG_INIT_JPG | IMG_INIT_PNG ) < 0 )
    {
        #ifdef __GERT_DEBUG__
        std::cout << IMG_GetError() << std::endl;
        #endif // __GERT_DEBUG__
        return 2;
    }
    if( TTF_Init() < 0 )
    {
        #ifdef __GERT_DEBUG__
        std::cout << TTF_GetError() << std::endl;
        #endif // __GERT_DEBUG__
        return 3;
    }
    if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 1024 ) < 0 )
    {
        #ifdef __GERT_DEBUG__
        std::cout << Mix_GetError() << std::endl;
        #endif // __GERT_DEBUG__
        return 4;
    }
    gCamera = { 0, 0, 1280, 720 };

    #ifdef __GERT_DEBUG__
    std::cout << "Loading saved settings...\n";
    #endif // __GERT_DEBUG__
    Uint32 wflag = SDL_WINDOW_SHOWN;
    if( load_options() )
    {
        wflag = SDL_WINDOW_FULLSCREEN_DESKTOP;
    }

    SDL_Window* gameWindow = SDL_CreateWindow( "Dad Slayer SIX", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gCamera.w, gCamera.h, wflag );
    gRenderer = SDL_CreateRenderer( gameWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
    /* I'd like to recommend making the game window global instead of the renderer since you can still access the renderer
    through the window by calling SDL_GetRenderer( gWindow ); I'll be using this for future projects instead of gRenderer*/

    SDL_GetWindowSize( gameWindow, &gCamera.w, &gCamera.h );

    #ifdef __GERT_DEBUG__
    std::cout << "Loading textures and GameManger...\n";
    #endif // __GERT_DEBUG__
    load_tex();
    gGameManager = new GameManager;
    #ifdef __GERT_DEBUG__
    std::cout << "Gamemanger made at " << gGameManager << std::endl;
    #endif // __GERT_DEBUG__

    Uint32 frameTime = 0;
    while( gGameManager->gameState != GameManager::gmsQUITTING )
    {
        gFrameTime = ( SDL_GetTicks() - frameTime ) * 0.001;
        frameTime = SDL_GetTicks();
        /* This will capture the time it took to proc and render the last frame, not a perfect way to do delta time processing
        but it's easy and still accurate if the framerate doesn't swing up and down. gFrameTime is based off of seconds in this game
        if an object has it's speed set to 300 then it moves 300 pixels per second.*/

        gGameManager->update();
        gGameManager->draw();

        #define MAX_FPS ( 1000.0/90 ) //Frame limiting is recommended, when you make a renderer and set the SDL_RENDERER_RESENTVSYNC flag it will limit automatically to vsync
        if( SDL_GetTicks() - frameTime < MAX_FPS )
        {
            SDL_Delay( MAX_FPS - ( SDL_GetTicks() - frameTime ) );
        }
    }

    close_tex();
    delete gGameManager;

    SDL_Quit();
    IMG_Quit();
    TTF_Quit();
    return 0;
}
