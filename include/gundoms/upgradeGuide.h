#ifndef UPGRADEGUIDE_H
#define UPGRADEGUIDE_H

namespace domUpgrades
{
    enum duNames
    {
        duMOVEMENT_SPEED,
        duKID_TIME,
        duAMMO_BONUS,
        duHEALTH,
        duTOTAL
    };
}

#endif // UPGRADEGUIDE_H
