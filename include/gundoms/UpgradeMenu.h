#ifndef UPGRADEMENU_H
#define UPGRADEMENU_H

#include "upgradeGuide.h"
#include <string>
class SDL_Rect;
class SDL_Point;
union SDL_Event;

class GameManager;
extern GameManager* gGameManager;

class UpgradeMenu
{
    public:
        UpgradeMenu( void );
        virtual ~UpgradeMenu( void );

        void draw( void );
        domUpgrades::duNames interact( const SDL_Point& mouse, const SDL_Event& event );
    private:
        int _inset;
        SDL_Rect* _clickboxs;
        std::string* _boxstrings;
};

#endif // UPGRADEMENU_H
