#ifndef GUNS_H
#define GUNS_H

class SDL_Point;
class GameManager;
extern GameManager* gGameManager;

namespace gunTypes
{
    enum gCodes
    {
        gcODDSHOT,
        gcTWOSPRAY,
        gcSLOWSHOT,
        gcLINE,
        gcNUKE,
        //gcSNIPER,
        gcTOTAL
    };

    void BasicPellet( const SDL_Point& pos, float angle, float* cooldown, bool dad );
    void OddShot( const SDL_Point& pos, float angle, float* cooldown, bool dad );
    void TwoSpray( const SDL_Point& pos, float angle, float* cooldown, bool dad );
    void SlowShot( const SDL_Point& pos, float angle, float* cooldown, bool dad );
    void Line( const SDL_Point& pos, float angle, float* cooldown, bool dad );
    void Nuke( const SDL_Point& pos, float angle, float* cooldown, bool dad );
    //void Sniper( const SDL_Point& pos, float angle, float* cooldown, bool dad );
}

#endif // GUNS_H
