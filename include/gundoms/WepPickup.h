#ifndef WEPPICKUP_H
#define WEPPICKUP_H

#include <mechanical/EnemyBase.h>
class HurtCircle;
struct SDL_Point;

class WepPickup : public EnemyBase
{
    public:
        WepPickup( void );
        virtual ~WepPickup( void );

        void draw( void );
        void update( void );
        void start( const SDL_Point& pos );

        void (*wep)( const SDL_Point&, float, float*, bool );
        int ammo;

        bool sated;
    protected:
        int _wepType;
        void _sync_all( void );
};

#endif // WEPPICKUP_H
