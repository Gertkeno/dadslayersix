#ifndef BULLET_H
#define BULLET_H

#include <mechanical/Actor.h>

struct SDL_Color;
class HitCircle;

extern double gFrameTime;

class Bullet : public Actor
{
    public:
        Bullet( void );
        virtual ~Bullet( void );

        void update( void );
        virtual void draw( void );
        void start( const SDL_Point& pos, double angle, bool dad, int damage );

        bool get_dead( void ) { return _dead; }
        void force_kill( void ) { _dead = true; }
        float deltaX, deltaY;
        bool piercing;

        SDL_Color* myColor;
    protected:
        void _sync_all( void );
        float _angle, _lifeTime;
        float _xBuffer, _yBuffer; // x and y buffers make everything better, more accurate and smooth.
        bool _dad, _dead;
        bool _collided;
        int _damage;

        HitCircle* _lastHit;
};

#endif // BULLET_H
