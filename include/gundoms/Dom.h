#ifndef DOM_H
#define DOM_H

#include <mechanical/EnemyBase.h>
#include "upgradeGuide.h"

struct SDL_Point;
union SDL_Event;

class Sound_Wrapper;
extern Sound_Wrapper* gSounds;

extern double gFrameTime;

class Turret;

class Dom : public EnemyBase
{
    public:
        Dom( void );
        virtual ~Dom( void );

        void interactive( const SDL_Event& event, const SDL_Point& mouse );
        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );

        void set_pickgun( void (*pickup)( const SDL_Point&, float, float*, bool ), int ammo );

        void upgrade( domUpgrades::duNames t );
        int get_upgradeProgress( domUpgrades::duNames t );
    private:
        void _sync_all( void );
        float _moveBufferX, _moveBufferY;

        //IMPROVEABLE STATS
        float _movementSpeed;
        float _turretTimer;
        int _ammoBonus;
        int _healthBonus;
            //_health

        //TURRET
        Turret* _myTurret;
        float _lastTurret;

        enum inputBools
        {
            ibFIRE,
            ibLEFT,
            ibRIGHT,
            ibUP,
            ibDOWN,
            ibTURRET_DROP,
            ibTOTAL
        };

        bool _inputs[ ibTOTAL ];

        //GUN STUFF
        float _gunCooldownTime, _gunCooldown, _storeGunAngle;
        int _ammo;
        void (*_gun)( const SDL_Point&, float, float*, bool );
        /* Pointers to functions are great, just has to be a static function */

        float _animationTiming;
};

#endif // DOM_H
