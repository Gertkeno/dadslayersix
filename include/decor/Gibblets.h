#ifndef GIBBLETS_H
#define GIBBLETS_H

#include "Bloods.h"

extern double gFrameTime;

class Gibblets : public Bloods
{
    public:
        Gibblets();
        virtual ~Gibblets();

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );
    protected:
        float _lifeTime;
        int _xDelta, _yDelta;
        /* Both _delta variables should be floats but I wanted to save 4 bytes per gibblet for some reason */
};

#endif // GIBBLETS_H
