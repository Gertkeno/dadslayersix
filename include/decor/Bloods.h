#ifndef BLOODS_H
#define BLOODS_H

#include <mechanical/Actor.h>


class Bloods : public Actor
{
    public:
        Bloods( void );
        virtual ~Bloods( void );

        virtual void update( void );
        virtual void draw( void );
        virtual void start( const SDL_Point& pos );

        bool alive;
    protected:
        float _angle;
        int _tile;

        void _sync_all( void );
};

#endif // BLOODS_H
