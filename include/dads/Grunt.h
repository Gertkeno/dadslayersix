#ifndef GRUNT_H
#define GRUNT_H

#include <dads/DadBase.h>

namespace dads
{
    class Grunt : public DadBase
    {
        public:
            Grunt();
            virtual ~Grunt();

            void update( void );
            void draw( void );
            void start( const SDL_Point& pos );

            float base_attackSpeed( void ) { return 0.5; }
            float base_movementSpeed( void ) { return 190; }
            float base_attackRange( void ) { return 50; }
        protected:

            void _sync_all( void );
    };
}

#endif // GRUNT_H
