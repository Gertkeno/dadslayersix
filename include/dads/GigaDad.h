#ifndef GIGADAD_H
#define GIGADAD_H

#include <dads/DadBase.h>

namespace dads
{
    class GigaDad : public DadBase
    {
        public:
            GigaDad();
            virtual ~GigaDad();

            float base_attackRange( void ) { return 80; }
            float base_movementSpeed( void ) { return 220; }
            float base_attackSpeed( void ) { return 0.6; }

            void draw( void );
            void update( void );
            void start( const SDL_Point& pos );
        private:
            int _leftHand, _rightHand, _head;
            SDL_Point* _bodyPoints;
            void _sync_all( void );
    };
}

#endif // GIGADAD_H
