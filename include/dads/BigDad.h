#ifndef BIGDAD_H
#define BIGDAD_H

#include <dads/DadBase.h>

namespace dads
{
    class BigDad : public DadBase
    {
        public:
            BigDad( void );
            virtual ~BigDad( void );

            void draw( void );
            void update( void);
            void start( const SDL_Point& pos );

            float base_attackSpeed( void ) { return 0.5; }
            float base_movementSpeed( void ) { return 160; }
            float base_attackRange( void ) { return 70; }
        private:
            void _sync_all( void );

            static void _buffSpeed( DadBase* t );
    };
}

#endif // BIGDAD_H
