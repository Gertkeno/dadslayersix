#ifndef GRUNTENEMY_H
#define GRUNTENEMY_H

#include <mechanical/EnemyBase.h>

class Dom;
class SDL_Color;
class HitCircle;

class Sound_Wrapper;
extern Sound_Wrapper* gSounds;

extern double gFrameTime;

namespace dads
{
    class DadBase : public EnemyBase
    {
        public:
            DadBase( void );
            virtual ~DadBase( void );

            virtual void draw( void );
            virtual void update( void );
            virtual void start( const SDL_Point& pos );

            //stats
            float movementSpeed;
            float attackSpeed;
            float attackRange;

            //base stats
            virtual float base_movementSpeed( void ) = 0;
            virtual float base_attackSpeed( void ) = 0;
            virtual float base_attackRange( void ) = 0;
            /* Base stats are virtual = 0 so they are basically static variables that change based on derived objects */

            bool get_revved( void ) { return _revved; }
        protected:
            bool _revved, _dead; /* dead variable just allows for one-time functions to be called on death, such as set blood;
            revved allows the wave to spit out dads at a certain rate but only initilizing then at start*/
            float _lastAttack;
            float _xBuffer, _yBuffer; // float values make fore more consistant movement
            SDL_Color* _myColor;
            bool _hit;
    };

    enum dadTypes
    {
        dGRUNT,
        dENGIDAD,
        dBIGDAD,
        dGIGADAD,
        dTOTAL
    };
}

#endif // GRUNTENEMY_H
