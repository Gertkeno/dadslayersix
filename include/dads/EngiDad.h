#ifndef ENGIDAD_H
#define ENGIDAD_H

#include <dads/DadBase.h>

class Turret;

namespace dads
{
    class EngiDad : public DadBase
    {
        public:
            EngiDad( void );
            virtual ~EngiDad( void );

            void draw( void );
            void update( void );
            void start( const SDL_Point& pos );

            float base_attackSpeed( void ) { return 0.8; }
            float base_movementSpeed( void ) { return 150; }
            float base_attackRange( void ) { return 50; }

            virtual void force_kill( int nh = 0 );
        protected:
            Turret* _myTurret;

            void _sync_all();
    };
}

#endif // ENGIDAD_H
