#ifndef TURRET_H
#define TURRET_H

#include <mechanical/EnemyBase.h>

class Dom;

class Sound_Wrapper;
extern Sound_Wrapper* gSounds;

class Turret : public EnemyBase
{
    public:
        friend class Dom;
        Turret( void );
        virtual ~Turret( void );

        void update( void );
        void draw( void );
        void start( const SDL_Point& pos );

        void (*mGun)( const SDL_Point&, float, float*, bool );
        float angle;
    private:
        void _sync_all( void );

        float _lastShot;
        float _cooldown;
};

#endif // TURRET_H
