#ifndef TEXHOLDER_H
#define TEXHOLDER_H

class MultiFrame_Wrap;
class Font_Wrapper;
class Sound_Wrapper;

extern MultiFrame_Wrap* gTextures;
extern Font_Wrapper* gFont;
//extern Sound_Wrapper** gSounds;

#include <sstream>

template<typename NN>
std::string ct_string( NN in )
{
    std::stringstream ss; ss << in;
    return ss.str();
}

#define FONT_H gFont->string_height()

/* Texture Holder namespace merely keeps track of which textures are in the array of textures.
If a game loads and deallocates many textures, basically any non-arcade style game,
then I'd recommend an array of pointers since they take 4 bytes instead of sizeof( MultiFrame_Wrap )*/

namespace TexHolder
{
    enum texnames
    {
        tCIRCLE,
        tRING,
        tBULLET,
        tCHEST,
        tDAD_GRUNT,
        tDAD_ENGI,

        tGIGA_HAND,
        tGIGA_HEAD,

        tDOM_BACK,
        tDOM_FRONT,
        tDOM_STANDING,
        tDOM_RIGHT,

        tBLOOD,
        tGIB,
        tWIRE,
        tTURRET,
        tBACKGROUND,
        tTOTAL
    };

    enum soundnames
    {
        s0DAD_SPAWN,
        s1DAD_SPAWN,
        s2DAD_SPAWN,
        s3DAD_SPAWN,
        s4DAD_SPAWN,

        s0DAD_HIT,

        s0DAD_SCREAM,
        s1DAD_SCREAM,
        s2DAD_SCREAM,

        sPLAYERDEATH,

        sPEW,
        sTOTAL
    };
}

#endif // TEXHOLDER_H
