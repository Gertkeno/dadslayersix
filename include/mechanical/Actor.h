#ifndef ACTOR_H
#define ACTOR_H

struct SDL_Point;
class GameManager;
extern GameManager* gGameManager;

class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;

class Circle;

class Actor
{
    public:
        Actor( void );
        virtual ~Actor( void );

        virtual void update( void ) = 0;
        virtual void draw( void );
        virtual void start( const SDL_Point& pos );

        SDL_Point get_syncPoint( void );
        Circle get_circ_from_point( float radius );
    protected:
        SDL_Point* _syncPoint;
        /* A point to sync against is great for large entities or anything with multiple parts that need to be connected */

        virtual void _sync_all( void ) = 0;
};

#endif // ACTOR_H
