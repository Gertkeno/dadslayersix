#ifndef HURTCIRCLE_H
#define HURTCIRCLE_H

class HitCircle;
class EnemyBase;
struct Circle;

class GameManager;
extern GameManager* gGameManager;

struct HurtCircle
{
    HurtCircle( void );
    virtual ~HurtCircle( void );

    bool check_collide( HitCircle* hc );
    int add_health( int nh );
    void set_pHealth( int* ph ) { _health = ph; }

    EnemyBase* owner;
    Circle* area;
protected:
    int* _health;
};

#endif // HURTCIRCLE_H
