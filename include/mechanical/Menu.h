#ifndef MENU_H
#define MENU_H

#include <string>

class SDL_Point;
class SDL_Rect;
union SDL_Event;

class MultiFrame_Wrap;
extern MultiFrame_Wrap* gTextures;

#define WINDOW_SIZES_TOTAL 4

class Menu
{
    public:
        Menu( void );
        virtual ~Menu( void );

        enum options
        {
            mCONTINUE,
            mNEW_GAME,
            mHIGHSCORES,
            mFULL_SCREEN,
            mWINDOWED_SIZE,
            mVOLUME_CONTROL,
            mMUSIC_VOLUME,
            mQUIT,
            mTOTAL
        };

        static const int* CLASSIC_WINDOW_SIZES;
        static float percentVolume, percentMusic;

        bool fullscreenTemp, restartReq;

        void draw( void );
        options interact( const SDL_Event& event, const SDL_Point& mouse );
        void resize_me( void );

        void save_settings( void );
    protected:
        SDL_Rect* _clickBoxs;
        std::string* _boxstrings;
        options _inset;
};

#endif // MENU_H
