#ifndef WAVEMANAGE_H
#define WAVEMANAGE_H

namespace dads
{
    class DadBase;
}
struct Circle;

struct SDL_Rect;
extern SDL_Rect gCamera;
struct SDL_Point;

extern double gFrameTime;

class WaveManage
{
    public:
        WaveManage( void );
        virtual ~WaveManage( void );

        void update( void );
        void draw( void );

        void start_wave( void );
        bool wave_over( void );
        void abort_wave( void );

        int apply_inRange( dads::DadBase* source, const Circle& area, void(*apply)( dads::DadBase* t ) );

        int get_totalDads( void ) { return _totalDads; }
        int get_dadsLeft( void );
    private:
        int _totalDads;
        double _ellapsedTime;
        int _revvedDads;
        dads::DadBase** _myDads;

        void _shuffle_dads( void );
};

#endif // WAVEMANAGE_H
