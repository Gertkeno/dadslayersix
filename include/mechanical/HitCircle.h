#ifndef HITCIRCLE_H
#define HITCIRCLE_H

struct Circle;
class Actor;

struct HitCircle
{
    HitCircle( void );
    virtual ~HitCircle( void );
    void start( Circle area, bool dad, int damage );

    Circle* area;
    int damage;
    bool dad;
    bool alive;
    bool* didCollide;
};

#endif // HITCIRCLE_H
