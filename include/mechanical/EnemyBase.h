#ifndef ENEMYBASE_H
#define ENEMYBASE_H

#include <mechanical/Actor.h>

#define HURTCIRCINIT _myHurtCircles = new HurtCircle*[ _hurtCircles ]; for( int i = 0; i < _hurtCircles; i++ ) _myHurtCircles = nullptr;
class HurtCircle;
extern double gFrameTime;

class EnemyBase: public Actor
{
    public:
        friend class HurtCircle;
        EnemyBase( void );
        virtual ~EnemyBase( void );

        bool is_dead( void ) { return _health <= 0; }
        virtual void force_kill( int nh = 0 ) { _health = nh; if( _health < 1 ) _pull_hurtCircles(); }
    protected:
        bool _dad;
        int _health;

        int _hurtCircles;
        HurtCircle** _myHurtCircles;
        void _pull_hurtCircles( void );
};

#endif // ENEMYBASE_H
