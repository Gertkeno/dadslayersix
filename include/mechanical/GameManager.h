#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H

class Circle;
class HitCircle;
class HurtCircle;
class EnemyBase;
class Bullet;
class Dom;
class UpgradeMenu;
class WepPickup;

class Bloods;
class Gibblets;

struct SDL_Point;
union SDL_Event;

class SDL_Renderer;
extern SDL_Renderer* gRenderer;

struct SDL_Rect;
extern SDL_Rect gCamera;

typedef class _Mix_Music Mix_Music;

class Menu;
class WaveManage;

class GameManager
{
    public:
        GameManager();
        virtual ~GameManager();

        enum gstate
        {
            gmsPLAYING,
            gmsMENU,
            gmsHIGHSCOREVIEW,
            gmsQUITTING
        };

        gstate gameState;
        bool get_inCombat( void ) { return _inCombat; }

        void update( void );
        void draw( void );

        void add_score( double b ) { _score += b; }
        double get_score( void ) { return _score; }

        //ENEMY WAVE
        WaveManage* waver;
        Dom* theDom;
        void set_blood( const SDL_Point& pos );
        void set_gib( const SDL_Point& pos );

        //COLLISION
        HitCircle* set_hitCircle( Circle spot, bool dad, int damage );
        HurtCircle* request_hurtCircle( EnemyBase* owner, int* phealth );
        void pull_hurtCircles( EnemyBase* rm );

        //BULLETS
        Bullet* set_bullet( SDL_Point spot, float angle, bool dad, int damage );
    private:
        //COLLISION
        HitCircle* _hitC;
        int _activeHitC;
        HurtCircle* _hurtC;

        //Decals
        Mix_Music* _musical;
        Bloods* _bleed;
        int _activeBleed;
        Gibblets* _gib;

        //BULLETS
        Bullet* _bullets;

        //DRRRRRRRRRRRROPS
        WepPickup* _drops;
        float _lastDrop;

        //COOL SDL STUFF
        SDL_Event* _thevent;
        SDL_Point* _mouse;

        //HIGHSCORING
        double _score;
        int* _highScores;
        int _currentHighScore;
        bool _exitHighScoreViewHeld;
        SDL_Rect* _clickExitHighScoreView;

        void _push_highscore( void );
        void _write_highscores( void );

        //MENU MECHANICALS
        Menu* _theMenu;
        UpgradeMenu* _theUpgradeMenu;
        bool _inCombat;
};

#endif // GAMEMANAGER_H
