#include "gundoms/Bullet.h"

#include <SDL2/SDL.h>
#include <mechanical/TexHolder.h>
#include <gert_Wrappers.h>
#include <mechanical/GameManager.h>
#include <mechanical/HitCircle.h>
#include <gert_Collision.h>
#include <gert_CameraMath.h>

#ifdef __GERT_DEBUG__
    #include <iostream>
#endif // __GERT_DEBUG__

#define BULLETSPEED 1300
#define RADIUS 12
#define BULLET_MAX_LIFE 1.8

Bullet::Bullet( void )
{
    //ctor
    deltaX = 0; deltaY = 0;
    _dad = false;
    _dead = true;
    _damage = 0;
    myColor = new SDL_Color( { 255, 255, 255, 255 } );
    _lastHit = nullptr;
    _lifeTime = 0;
    piercing = false;
    _collided = false;
}

Bullet::~Bullet( void )
{
    //dtor
    delete myColor;
}

void Bullet::update( void )
{
    if( !_dead )
    {
        _lifeTime += gFrameTime;
        if( ( !piercing && ( _collided ) ) || ( _lifeTime > BULLET_MAX_LIFE ) )
        {
            _dead = true;
        }
        else
        {
            _xBuffer += deltaX * gFrameTime;
            _yBuffer += deltaY * gFrameTime;

            _lastHit = gGameManager->set_hitCircle( get_circ_from_point( RADIUS ), _dad, _damage );
            _lastHit->didCollide = &_collided;
        }
        _sync_all();
    }
}

void Bullet::draw( void )
{
    if( !_dead )
    {
        SDL_Rect foo = { _syncPoint->x - RADIUS, _syncPoint->y - RADIUS/2, RADIUS*2, RADIUS };
        gTextures[ TexHolder::tBULLET ].render( foo, *myColor, 0, _angle );
    }
    Actor::draw();
}

void Bullet::_sync_all( void )
{
    _syncPoint->x = _xBuffer;
    _syncPoint->y = _yBuffer;
}

void Bullet::start( const SDL_Point& pos, double angle, bool dad, int damage )
{
    Actor::start( pos );
    _xBuffer = _syncPoint->x;
    _yBuffer = _syncPoint->y;

    _angle = angle;
    angle = M_PI/180 * angle;
    deltaX = cos( angle ) * BULLETSPEED;
    deltaY = sin( angle ) * BULLETSPEED;

    _dad = dad;
    _damage = damage;
    _dead = false;
    _lastHit = nullptr;
    _collided = false;
    _lifeTime = 0;

    piercing = false;

    #ifdef __GERT_DEBUG__
    //std::cout << "Bullet at " << pos.x << ", " << pos.y << "\tAngle " << angle << std::endl;
    #endif // __GERT_DEBUG__
}
