#include "gundoms/UpgradeMenu.h"

#include <SDL2/SDL.h>
#include <gert_Collision.h>
#include <mechanical/TexHolder.h>
#include <gert_Wrappers.h>
#include <gert_CameraMath.h>
#include <gert_FontWrapper.h>

#include <mechanical/GameManager.h>
#include <gundoms/Dom.h>

#include <sstream>

extern MultiFrame_Wrap* gTextures;
using namespace domUpgrades;

UpgradeMenu::UpgradeMenu( void )
{
    //ctor
    _inset = duTOTAL;
    _boxstrings = new std::string[ duTOTAL ];
    _boxstrings[ duAMMO_BONUS ] = "More Ammo _/_";
    _boxstrings[ duHEALTH ] = "Health _/_";
    _boxstrings[ duMOVEMENT_SPEED ] = "Speed _/_";
    _boxstrings[ duKID_TIME ] = "Turret CD _/_";

    _clickboxs = new SDL_Rect[ duTOTAL ];
    for( int i = 0; i < duTOTAL; i++ )
    {
        _clickboxs[ i ] = rectAlign::right( { 0, FONT_H * ( i + 2 ) + i * 2, gFont->string_width( _boxstrings[ i ] ), FONT_H }, gCamera );
    }
}

UpgradeMenu::~UpgradeMenu( void )
{
    //dtor
    delete[] _clickboxs;
    delete[] _boxstrings;
}

#define DRAW_TEXT( n )\
    ctProper = _boxstrings[ n ];\
    ctProper = ctProper.substr( 0, ctProper.find( '_' ) );\
    ctProper.append( ct_string( gGameManager->theDom->get_upgradeProgress( n ) ) );\
    ctProper.append( "/8" );\
    gFont->render( _clickboxs[ n ], ctProper, color::BLACK );

void UpgradeMenu::draw( void )
{
    for( int i = 0; i < duTOTAL; i++ )
    {
        SDL_Color c = color::WHITE;
        if( _inset == i )
        {
            c = color::CYAN;
        }
        gTextures[ TexHolder::tBULLET ].render( _clickboxs[ i ], c );
    }
    std::string ctProper;
    DRAW_TEXT( duAMMO_BONUS )
    DRAW_TEXT( duKID_TIME )
    DRAW_TEXT( duMOVEMENT_SPEED )
    DRAW_TEXT( duHEALTH )
}

duNames UpgradeMenu::interact( const SDL_Point& mouse, const SDL_Event& event )
{
    switch( event.type )
    {
    case SDL_MOUSEBUTTONDOWN:
        for( int i = 0; i < duTOTAL; i++ )
        {
            if( collision::get_collide( Circle{ float( mouse.x ), float( mouse.y ), 1 }, _clickboxs[ i ] ) )
            {
                _inset = i;
                break;
            }
        }
        break;
    case SDL_MOUSEBUTTONUP:
        for( int i = 0; i < duTOTAL; i++ )
        {
            if( _inset == i && collision::get_collide( Circle{ float( mouse.x ), float( mouse.y ), 1 }, _clickboxs[ i ] ) )
            {
                _inset = duTOTAL;
                return duNames( i );
            }
        }
        _inset = duTOTAL;
        break;
    }
    return duTOTAL;
}
