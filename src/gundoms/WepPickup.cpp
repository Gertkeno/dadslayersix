#include "gundoms/WepPickup.h"

#include <SDL2/SDL.h>
#include <mechanical/HurtCircle.h>
#include <mechanical/GameManager.h>
#include <gert_Collision.h>
#include <gert_Wrappers.h>
#include <mechanical/TexHolder.h>
#include <gundoms/Guns.h>
#include <random>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

WepPickup::WepPickup( void )
{
    //ctor
    wep = nullptr;
    ammo = 0;
    _wepType = gunTypes::gcTOTAL;
    _hurtCircles = 1;
    sated = true;
    //KEEP SAME
    _dad = true;
    _myHurtCircles = new HurtCircle*;
}

WepPickup::~WepPickup( void )
{
    //dtor
    delete _myHurtCircles;
}

void WepPickup::update( void )
{
    if( _health <= 0 )
    {
        _pull_hurtCircles();
    }
}

void WepPickup::draw( void )
{
    if( !is_dead() )
    {
        #define WH 30
        SDL_Rect foo = { _syncPoint->x - WH/2, _syncPoint->y - WH/2, WH, WH };
        SDL_Color c = color::WHITE;
        switch( _wepType )
        {
        case gunTypes::gcLINE:
            c = color::BLUE;
            break;
        case gunTypes::gcODDSHOT:
            c = color::CYAN;
            break;
        case gunTypes::gcSLOWSHOT:
            c = color::RED;
            break;
        case gunTypes::gcTWOSPRAY:
            c = color::GREEN;
            break;
        case gunTypes::gcNUKE:
            c = color::MAGENTA;
            break;
//        case gunTypes::gcSNIPER:
//            c = color::BLACK;
//            break;
        }
        gTextures[ TexHolder::tCHEST ].render( foo, c );
    }
    Actor::draw();
}

void WepPickup::_sync_all( void )
{

}

void WepPickup::start( const SDL_Point& pos )
{
    Actor::start( pos );
    sated = false;
    _health = 3;

    _pull_hurtCircles();
    *_myHurtCircles = gGameManager->request_hurtCircle( this, &_health );
    *(*_myHurtCircles)->area = get_circ_from_point( WH/2 );

    _wepType = rand()%gunTypes::gcTOTAL;
    switch( _wepType )
    {
    case gunTypes::gcLINE:
        wep = gunTypes::Line;
        ammo = 20;
        break;
    case gunTypes::gcODDSHOT:
        wep = gunTypes::OddShot;
        ammo = 30;
        break;
    case gunTypes::gcSLOWSHOT:
        wep = gunTypes::SlowShot;
        ammo = 60;
        break;
    case gunTypes::gcTWOSPRAY:
        wep = gunTypes::TwoSpray;
        ammo = 45;
        break;
    case gunTypes::gcNUKE:
        wep = gunTypes::Nuke;
        ammo = 3;
        break;
//    case gunTypes::gcSNIPER:
//        wep = gunTypes::Sniper;
//        ammo = 5;
//        break;
    }
}
