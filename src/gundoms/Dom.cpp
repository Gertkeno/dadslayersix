#include "gundoms/Dom.h"

#include <SDL2/SDL.h>
#include <mechanical/TexHolder.h>

#include <gert_FontWrapper.h>
#include <gert_Wrappers.h>
#include <gert_CameraMath.h>
#include <gert_SoundWrapper.h>
#include <gert_Collision.h>

#include <mechanical/HurtCircle.h>
#include <Turret.h>

#include <mechanical/GameManager.h>
#include <gundoms/Guns.h>

#define DOM_WIDTH 20
#define DOM_DRAW_WIDTH 40
#define DOM_DRAW_HEIGHT 60
#define MAX_HEALTH 50

#define DOM_ANIMATION_TIME 0.6

#define RELEASE_TIME 2
#ifdef __GERT_DEBUG__
    #define BASE_KID_TIME 8.0

    #include <iostream>
#else
    #define BASE_KID_TIME 35.0
#endif // __GERT_DEBUG__

#define BASE_MOVEMENT 200.0

Dom::Dom( void )
{
    //ctor
    _ammo = 0;
    _gun = gunTypes::BasicPellet;
    _dad = false;
    _myHurtCircles = new HurtCircle*;

    for( int i = 0; i < ibTOTAL; i++ )
    {
        _inputs[ i ] = false;
    }
    _gunCooldown = 0;
    _gunCooldownTime = 0;
    _storeGunAngle = 0;
    _moveBufferX = 0; _moveBufferY = 0;

    //TURRETSTUFF
    _lastTurret = 0.0;
    _myTurret = new Turret;

    //base stats
    _movementSpeed = BASE_MOVEMENT;
    _turretTimer = BASE_KID_TIME;
    _ammoBonus = 0;
    _healthBonus = 0;
}

Dom::~Dom( void )
{
    //dtor
    delete _myTurret;
}

void Dom::update( void )
{
    if( !is_dead() )
    {
        //GUN STUFF
        _animationTiming += gFrameTime;
        if( _animationTiming >= DOM_ANIMATION_TIME )
        {
            _animationTiming = 0;
        }

        _gunCooldown += gFrameTime;
        if( _inputs[ ibFIRE ] )
        {
            if( _gunCooldown > _gunCooldownTime )
            {
                _gun( *_syncPoint, _storeGunAngle, &_gunCooldownTime, false );
                gSounds[ TexHolder::sPEW ].play();
                if( gGameManager->get_inCombat() && --_ammo < 0 )
                {
                    _gun = gunTypes::BasicPellet;
                }
                _gunCooldown = 0;
            }
        }

        //TURRET STUFF
        if( gGameManager->get_inCombat() )
        {
            _lastTurret += gFrameTime;
        }
        if( _inputs[ ibTURRET_DROP ] && _lastTurret > _turretTimer )
        {
            _myTurret->start( *_syncPoint );
            _myTurret->mGun = _gun;
            _ammo = 0;
            _gun = gunTypes::BasicPellet;

            _myTurret->angle = _storeGunAngle;
            _myTurret->_dad = false;
            _lastTurret = 0.0;
        }

        _myTurret->update();

        //MOVEMENT
        float xmov = 0, ymov = 0;
        if( _inputs[ ibLEFT ] )
        {
            xmov = -1;
        }
        else if( _inputs[ ibRIGHT ] )
        {
            xmov = 1;
        }

        if( _inputs[ ibUP ] )
        {
            ymov = -1;
        }
        else if( _inputs[ ibDOWN ] )
        {
            ymov = 1;
        }

        if( ymov != 0 && xmov != 0 )
        {
            ymov *= 2.0/3.0;
            xmov *= 2.0/3.0;
        }

        _moveBufferX += _movementSpeed * gFrameTime * xmov;
        _moveBufferY += _movementSpeed * gFrameTime * ymov;
        if( _moveBufferX < gCamera.x ) _moveBufferX = gCamera.x;
        if( _moveBufferX > gCamera.x + gCamera.w ) _moveBufferX = gCamera.x + gCamera.w;
        if( _moveBufferY < gCamera.y ) _moveBufferY = gCamera.y;
        if( _moveBufferY > gCamera.y + gCamera.h ) _moveBufferY = gCamera.y + gCamera.h;

        _sync_all();
    }//END ALIVE
    else
    {
        _pull_hurtCircles();
        _myTurret->force_kill();
    }
}

void Dom::draw( void )
{
    if( !is_dead() )
    {
        _myTurret->draw();

        SDL_RendererFlip f = SDL_FLIP_NONE;
        if( _inputs[ ibLEFT ] )
        {
            f = SDL_FLIP_HORIZONTAL;
        }

        int textype = TexHolder::tDOM_STANDING;
        if( _inputs[ ibUP ] )
        {
            textype = TexHolder::tDOM_BACK;
        }
        else if( _inputs[ ibDOWN ] )
        {
            textype = TexHolder::tDOM_FRONT;
        }
        else if( _inputs[ ibRIGHT ] || _inputs[ ibLEFT ] )
        {
            textype = TexHolder::tDOM_RIGHT;
        }

        int frameN = _animationTiming / DOM_ANIMATION_TIME * gTextures[ textype ].get_maxFrames();

        if( !( _inputs[ ibUP ] || _inputs[ ibDOWN ] || _inputs[ ibRIGHT ] || _inputs[ ibLEFT ] ) )
        {
            frameN = 0;
        }

        gTextures[ textype ].render( { _syncPoint->x - DOM_DRAW_WIDTH/2, _syncPoint->y - DOM_DRAW_HEIGHT/2, DOM_DRAW_WIDTH, DOM_DRAW_HEIGHT }, color::WHITE, frameN, 0, f );

        if( _ammo > 0 )
        {
            SDL_Rect textp = { _syncPoint->x - DOM_DRAW_WIDTH/2, _syncPoint->y - DOM_DRAW_HEIGHT/2 - FONT_H, 200, 2 };
            gFont->render( textp, ct_string( _ammo ), color::CYAN );
        }
        if( _health > 0 )
        {
            SDL_Rect textp = { _syncPoint->x - DOM_DRAW_WIDTH/2, _syncPoint->y + DOM_DRAW_HEIGHT/2, 200, 2 };
            gFont->render( textp, ct_string( _health ), color::RED );
        }
        if( _turretTimer - _lastTurret > 0 )
        {
            SDL_Rect textp = { _syncPoint->x + DOM_DRAW_WIDTH/2, _syncPoint->y - FONT_H/2, 200, 2 };
            gFont->render( textp, ct_string<int>( _turretTimer - _lastTurret ), color::GREEN );
        }
    }
    Actor::draw();
}

void Dom::_sync_all( void )
{
    _syncPoint->x = _moveBufferX;
    _syncPoint->y = _moveBufferY;
    *(*_myHurtCircles)->area = get_circ_from_point( DOM_WIDTH );
}

void Dom::interactive( const SDL_Event& event, const SDL_Point& mouse )
{
    _storeGunAngle = angles::get_angle( add_camera( *_syncPoint ), mouse );
    switch( event.type )
    {
    case SDL_KEYDOWN:
        switch( event.key.keysym.sym )
        {
        case SDLK_s:
            _inputs[ ibDOWN ] = true;
            break;
        case SDLK_w:
            _inputs[ ibUP ] = true;
            break;
        case SDLK_a:
            _inputs[ ibLEFT ] = true;
            break;
        case SDLK_d:
            _inputs[ ibRIGHT ] = true;
            break;
        case SDLK_SPACE:
            _inputs[ ibTURRET_DROP ] = true;
            break;
        }
        break;//END KEYDOWN
    case SDL_KEYUP:
        switch( event.key.keysym.sym )
        {
        case SDLK_s:
            _inputs[ ibDOWN ] = false;
            break;
        case SDLK_w:
            _inputs[ ibUP ] = false;
            break;
        case SDLK_a:
            _inputs[ ibLEFT ] = false;
            break;
        case SDLK_d:
            _inputs[ ibRIGHT ] = false;
            break;
        case SDLK_SPACE:
            _inputs[ ibTURRET_DROP ] = false;
            break;
        }
        break; // END KEYUP
    case SDL_MOUSEBUTTONDOWN:
        switch( event.button.button )
        {
        case SDL_BUTTON_LEFT:
            _inputs[ ibFIRE ] = true;
            break;
        }
        break;//END MOUSEBUTTONDOWN
    case SDL_MOUSEBUTTONUP:
        switch( event.button.button )
        {
        case SDL_BUTTON_LEFT:
            _inputs[ ibFIRE ] = false;
            break;
        }
        break;//END MOUSEBUTTONUP
    }//END EVENT TYPE
}

void Dom::start( const SDL_Point& pos )
{
    Actor::start( pos );
    _moveBufferX = _syncPoint->x;
    _moveBufferY = _syncPoint->y;

    //STATS
    _health = MAX_HEALTH;
    _movementSpeed = BASE_MOVEMENT;
    _ammoBonus = 0;
    _healthBonus = 0;

    _gun = gunTypes::BasicPellet;
    _ammo = 0;

    //TURRET
    _lastTurret = 0.0;
    _turretTimer = BASE_KID_TIME;
    _myTurret->force_kill();

    _pull_hurtCircles();

    *_myHurtCircles = gGameManager->request_hurtCircle( this, &_health );
    _sync_all();

    _animationTiming = 0.0;
}

void Dom::set_pickgun( void (*pickup)( const SDL_Point&, float, float*, bool ), int ammo )
{
    if( !is_dead() )
    {
        _gun = pickup;
        _ammo = ammo + ( float( ammo )/4 * _ammoBonus );
        _gunCooldownTime = 0;
    }
}

#define COST 300

void Dom::upgrade( domUpgrades::duNames t )
{
    using namespace domUpgrades;
    double cash = gGameManager->get_score();
    bool payout = false;
    if( cash >= COST )
    {
        switch( t )
        {
        case duAMMO_BONUS:
            if( _ammoBonus < 8 )
            {
                _ammoBonus += 1;
                payout = true;
            }
            break;
        case duHEALTH:
            if( _healthBonus < 8 )
            {
                _health += 10;
                _healthBonus++;
                payout = true;
            }
            break;
        case duKID_TIME:
            if( _turretTimer > BASE_KID_TIME/2 )
            {
                _turretTimer -= BASE_KID_TIME/2/8;
                payout = true;
            }
            break;
        case duMOVEMENT_SPEED:
            if( _movementSpeed < BASE_MOVEMENT * 2 )
            {
                _movementSpeed += BASE_MOVEMENT/8;
                payout = true;
            }
            break;
        case duTOTAL:
            payout = false;
            break;
        }
        if( payout )
        {
            gGameManager->add_score( -COST );
        }
    }
}

int Dom::get_upgradeProgress( domUpgrades::duNames t )
{
    using namespace domUpgrades;
    switch( t )
    {
    case duAMMO_BONUS:
        return _ammoBonus;
    case duHEALTH:
        return _healthBonus;
    case duKID_TIME:
        return ( BASE_KID_TIME - _turretTimer )/( BASE_KID_TIME/2 ) * 8;
    case duMOVEMENT_SPEED:
        return float( _movementSpeed - BASE_MOVEMENT )/( BASE_MOVEMENT ) * 8;
    default:
        break;
    }
    return 0;
}
