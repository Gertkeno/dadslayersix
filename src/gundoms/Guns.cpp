#include <gundoms/Guns.h>

#include <SDL2/SDL.h>
#include <gert_Wrappers.h>
#include <mechanical/GameManager.h>
#include <gundoms/Bullet.h>
#include <cmath>
#include <random>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

namespace gunTypes
{
    void BasicPellet( const SDL_Point& pos, float angle, float* cooldown, bool dad )
    {
        Bullet* foo = gGameManager->set_bullet( pos, angle, dad, 2 );
        if( foo != nullptr )
        {
            *foo->myColor = color::YELLOW;
        }

        *cooldown = 0.2;
    }

    void OddShot( const SDL_Point& pos, float angle, float* cooldown, bool dad )
    {
        #define ODDSHOT_SHOTS 7
        Bullet* foo[ ODDSHOT_SHOTS ];
        for( int i = 0; i < ODDSHOT_SHOTS ; i++ )
        {
            foo[ i ] = gGameManager->set_bullet( pos, angle + i * 360/ODDSHOT_SHOTS, dad, 3 );
            if( foo[ i ] != nullptr )
            {
                *foo[ i ]->myColor = color::CYAN;
            }
        }

        *cooldown = 0.18;
    }

    void TwoSpray( const SDL_Point& pos, float angle, float* cooldown, bool dad )
    {
        #define TWOSPRAY_SHOTS 2
        Bullet* foo[ TWOSPRAY_SHOTS ];
        for( int i = 0; i < TWOSPRAY_SHOTS; i++ )
        {
            foo[ i ] = gGameManager->set_bullet( pos, angle + i * 20 - 10, dad, 2 );
            if( foo[ i ] != nullptr )
            {
                *foo[ i ]->myColor = color::GREEN;
            }
        }

        *cooldown = 0.08;
    }

    void SlowShot( const SDL_Point& pos, float angle, float* cooldown, bool dad )
    {
        #define SLOWSHOTS 2
        Bullet* foo[ SLOWSHOTS ];
        for( int i = 0; i < SLOWSHOTS; i++ )
        {
            foo[ i ] = gGameManager->set_bullet( pos, angle + rand()%60 - 30, dad, 1 );
            if( foo [ i ] != nullptr )
            {
                *foo[ i ]->myColor = color::RED;
                foo[ i ]->deltaX /= 2.5;
                foo[ i ]->deltaY /= 2.5;
            }
        }

        *cooldown = 0.04;
    }

    void Line( const SDL_Point& pos, float angle, float* cooldown, bool dad )
    {
        #define LINESHOTS 4
        Bullet* foo[ LINESHOTS ];
        for( int i = 0; i < LINESHOTS; i++ )
        {
            foo[ i ] = gGameManager->set_bullet( { pos.x + i * 15 - 20, pos.y + i * 15 - 20 }, angle, dad, 3 );
            if( foo[ i ] != nullptr )
            {
                *foo[ i ]->myColor = color::BLUE;
            }
        }

        *cooldown = 0.35;
    }

    void Nuke( const SDL_Point& pos, float angle, float* cooldown, bool dad )
    {
        #define NUKESHOTS 38
        Bullet* foo[ NUKESHOTS ];
        for( int i = 0; i < NUKESHOTS; i++ )
        {
            foo[ i ] = gGameManager->set_bullet( pos, angle + i*360/NUKESHOTS, dad, 5 );
            if( foo[ i ] != nullptr )
            {
                *foo[ i ]->myColor = color::MAGENTA;
            }
        }

        *cooldown = 0.8;
    }

//    void Sniper( const SDL_Point& pos, float angle, float* cooldown, bool dad )
//    {
//        Bullet* foo;
//        foo = gGameManager->set_bullet( pos, angle, dad, 8 );
//        if( foo != nullptr )
//        {
//            *foo->myColor = color::BLACK;
//            foo->deltaX *= 1.5;
//            foo->deltaY *= 1.5;
//            foo->piercing = true;
//        }
//
//        *cooldown = 0.9;
//    }
}
