#include "mechanical/HitCircle.h"

#include <gert_Collision.h>

HitCircle::HitCircle( void )
{
    //ctor
    area = new Circle( { 0, 0, 0 } );
    alive = false;
    damage = 0;
    dad = false;
    didCollide = nullptr;
}

HitCircle::~HitCircle( void )
{
    //dtor
    delete area;
}

void HitCircle::start( Circle area, bool dad, int damage )
{
    alive = true;
    didCollide = nullptr;
    *this->area = area;
    this->dad = dad;
    this->damage = damage;
}
