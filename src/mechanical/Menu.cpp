#include "mechanical/Menu.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <gert_CameraMath.h>
#include <mechanical/TexHolder.h>
#include <gert_FontWrapper.h>
#include <gert_Wrappers.h>
#include <gert_Collision.h>

#include <sstream>
#include <iomanip>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

Menu::Menu( void )
{
    //ctor
    _inset = mTOTAL;
    _clickBoxs = new SDL_Rect[ mTOTAL ];
    _boxstrings = new std::string[ mTOTAL ];
    _boxstrings[ mCONTINUE ] = "Continue";
    _boxstrings[ mNEW_GAME ] = "New Game";
    _boxstrings[ mHIGHSCORES ] = "High Scores";
    _boxstrings[ mFULL_SCREEN ] = "Full Screen: OFF";
    _boxstrings[ mWINDOWED_SIZE ] = "Window: 0000x0000";
    _boxstrings[ mVOLUME_CONTROL ] = "Volume Control: 00%";
    _boxstrings[ mMUSIC_VOLUME ] = "Music Volume: 00%";
    _boxstrings[ mQUIT ] = "Quit";

    restartReq = false;
    fullscreenTemp = false;
    SDL_RWops* settingsFile = SDL_RWFromFile( "settings.ds6", "r" );
    if( settingsFile != NULL )
    {
        SDL_RWread( settingsFile, &fullscreenTemp, sizeof( bool ), 1 );
        SDL_RWclose( settingsFile );
    }

    resize_me();
}

Menu::~Menu( void )
{
    //dtor
    delete[] _boxstrings;
    delete[] _clickBoxs;
}

void Menu::resize_me( void )
{
    std::stringstream sizeWindow; sizeWindow << "Window: ";
    sizeWindow << gCamera.w << "x" << gCamera.h;
    _boxstrings[ mWINDOWED_SIZE ] = sizeWindow.str();

    std::stringstream fscreen; fscreen << "Fullscreen: ";
    if( fullscreenTemp )
    {
        fscreen << "ON";
    }
    else
    {
        fscreen << "OFF";
    }
    _boxstrings[ mFULL_SCREEN ] = fscreen.str();

    std::stringstream vc; vc << "Volume Control: " << std::setprecision( 3 ) << percentVolume * 100 << "%";
    _boxstrings[ mVOLUME_CONTROL ] = vc.str();

    std::stringstream mv; mv << "Music Volume: " << std::setprecision( 3 ) << percentMusic * 100 << "%";
    _boxstrings[ mMUSIC_VOLUME ] = mv.str();

    for( int i = 0; i < mTOTAL; i++ )
    {
        _clickBoxs[ i ] = rectAlign::middle( { 0, i * FONT_H + ( i * 3 ), gFont->string_width( _boxstrings[ i ] ), FONT_H }, add_camera( gCamera, true ) );
    }
}

Menu::options Menu::interact( const SDL_Event& event, const SDL_Point& mouse )
{
    switch( event.type )
    {
    case SDL_MOUSEBUTTONDOWN:
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            bool noin = true;
            for( int i = 0; i < mTOTAL; i++ )
            {
                if( collision::get_collide( Circle{ float( mouse.x ), float( mouse.y ), 1 }, _clickBoxs[ i ] ) )
                {
                    _inset = options( i );
                    noin = false;
                    if( i == mVOLUME_CONTROL )
                    {
                        percentVolume = ( float( mouse.x - _clickBoxs[ i ].x ) / float( _clickBoxs[ i ].w ) );
                        resize_me();
                        Mix_Volume( -1, MIX_MAX_VOLUME * percentVolume );
                        #ifdef __GERT_DEBUG__
                        //std::cout << percentVolume << std::endl;
                        #endif // _GERT_DEBUG
                    }
                    else if( i == mMUSIC_VOLUME )
                    {
                        percentMusic = ( float( mouse.x - _clickBoxs[ i ].x ) / float( _clickBoxs[ i ].w ) );
                        resize_me();
                        Mix_VolumeMusic( MIX_MAX_VOLUME * percentMusic );
                    }
                }
            }
            if( noin ) _inset = mTOTAL;
        }
        break;
    case SDL_MOUSEBUTTONUP:
        if( event.button.button == SDL_BUTTON_LEFT )
        {
            for( int i = 0; i < mTOTAL; i++ )
            {
                if( _inset == i && collision::get_collide( Circle{ float( mouse.x ), float( mouse.y ), 1 }, _clickBoxs[ i ] ) )
                {
                    _inset = mTOTAL;
                    return options( i );
                }
            }
            _inset = mTOTAL;
        }
        break;
    }

    return mTOTAL;
}

#define EASYWRITE( p, s ) SDL_RWwrite( settingsFile, &p, 1, sizeof( s ) );
void Menu::save_settings( void )
{
    SDL_RWops* settingsFile = SDL_RWFromFile( "settings.ds6", "w" );
    if( settingsFile != NULL )
    {
        EASYWRITE( fullscreenTemp, bool )
        EASYWRITE( gCamera.w, int )
        EASYWRITE( gCamera.h, int )
        EASYWRITE( percentVolume, float )
        EASYWRITE( percentMusic, float )

        SDL_RWclose( settingsFile );
    }
    else
    {

    }
}

void Menu::draw( void )
{
    for( int i = 0; i < mTOTAL; i++ )
    {
        SDL_Color c = color::WHITE;
        if( _inset == i )
        {
            c = color::CYAN;
        }
        gTextures[ TexHolder::tBULLET ].render( _clickBoxs[ i ], c );
        gFont->render( _clickBoxs[ i ], _boxstrings[ i ], color::BLACK );
    }

    SDL_Rect vcLine = { _clickBoxs[ mVOLUME_CONTROL ].x + int( _clickBoxs[ mVOLUME_CONTROL ].w * percentVolume ), _clickBoxs[ mVOLUME_CONTROL ].y, 3, 9 };
    gFont->render( vcLine, "I", color::RED );
    vcLine.x = _clickBoxs[ mMUSIC_VOLUME ].x + int( _clickBoxs[ mMUSIC_VOLUME ].w * percentMusic );
    vcLine.y = _clickBoxs[ mMUSIC_VOLUME ].y;
    gFont->render( vcLine, "I", color::RED );

    #define RESTART_REQ_STRING "Changes require restarting!"
    if( restartReq )
    {
        SDL_Rect rrWarningRect = rectAlign::middle( { 0, _clickBoxs[ mQUIT ].y + FONT_H, gFont->string_width( RESTART_REQ_STRING ), FONT_H }, gCamera );

        gFont->render( rrWarningRect, RESTART_REQ_STRING, color::RED );
    }
}

const int* Menu::CLASSIC_WINDOW_SIZES = new int[ WINDOW_SIZES_TOTAL * 2 ]{ 800, 600,/**/ 1280, 720,/**/ 1600, 900,/**/ 1920, 1080 };
float Menu::percentVolume = 1, Menu::percentMusic = 1;
