#include "mechanical/EnemyBase.h"

#include <mechanical/GameManager.h>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

EnemyBase::EnemyBase( void )
{
    //ctor
    _dad = true;
    _health = 0;
    _myHurtCircles = nullptr;
    _hurtCircles = 0;
}

EnemyBase::~EnemyBase( void )
{
    //dtor
}

void EnemyBase::_pull_hurtCircles( void )
{
    gGameManager->pull_hurtCircles( this );
    for( int i = 0; i < _hurtCircles; i++ )
    {
        _myHurtCircles[ i ] = nullptr;
    }
}
