#include "mechanical/HurtCircle.h"

#include <mechanical/HitCircle.h>
#include <mechanical/EnemyBase.h>
#include <gert_Collision.h>
#include <SDL2/SDL.h>
#include <mechanical/GameManager.h>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

HurtCircle::HurtCircle( void )
{
    //ctor
    owner = nullptr;
    area = new Circle( { 0, 0, 0 } );
}

HurtCircle::~HurtCircle( void )
{
    //dtor
    delete area;
}

bool HurtCircle::check_collide( HitCircle* hc )
{
    if( owner != nullptr && owner->_dad != hc->dad && collision::get_collide( *area, *hc->area ) )
    {
        add_health( -1 * hc->damage );
        if( hc->didCollide != nullptr )
        {
            *hc->didCollide = true;
        }

        float normalX, normalY;
        collision::get_normal_diffXY( { int( area->x ), int( area->y ) }, { int( hc->area->x ), int( hc->area->y ) }, &normalX, &normalY );
        gGameManager->set_gib( { int( -normalX * area->r + area->x ), int( -normalY * area->r + area->y ) } );

        #ifdef __GERT_DEBUG__
        //std::cout << "Collision with (hurt)" << this << "\t(hit)" << hc << std::endl;;
        #endif // __GERT_DEBUG__

        return true;
    }
    return false;
}

int HurtCircle::add_health( int nh )
{
    *_health += nh;
    return *_health;
}
