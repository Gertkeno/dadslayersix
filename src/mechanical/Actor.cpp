#include "mechanical/Actor.h"

#include <SDL2/SDL.h>
#include <gert_Collision.h>

#ifdef __GERT_DEBUG__
extern SDL_Renderer* gRenderer;
extern SDL_Rect gCamera;

#include <iostream>
#endif // __GERT_DEBUG__

Actor::Actor( void )
{
    //ctor
    SDL_Point temp = { 0, 0 };
    _syncPoint = new SDL_Point( temp );
}

Actor::~Actor( void )
{
    //dtor
    delete _syncPoint;
}

void Actor::start( const SDL_Point& pos )
{
    *_syncPoint = pos;
}

SDL_Point Actor::get_syncPoint( void )
{
    return *_syncPoint;
}

Circle Actor::get_circ_from_point( float radius )
{
    return { float( _syncPoint->x ), float( _syncPoint->y ), radius };
}

void Actor::draw( void )
{
    #ifdef __GERT_DEBUG__
    SDL_SetRenderDrawColor( gRenderer, 255, 0, 0, 255 );
    SDL_RenderDrawPoint( gRenderer, _syncPoint->x - gCamera.x, _syncPoint->y - gCamera.y );
    #endif // __GERT_DEBUG__
}
