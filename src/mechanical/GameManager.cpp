#include "mechanical/GameManager.h"

#ifdef __GERT_DEBUG__
    #include <iostream>
#endif // __GERT_DEBUG__

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <random>
#include <mechanical/Menu.h>
#include <gundoms/UpgradeMenu.h>
#include <sstream>

#include <gert_Wrappers.h>
#include <gert_CameraMath.h>
#include <gert_Collision.h>
#include <gert_SoundWrapper.h>
#include <mechanical/TexHolder.h>
#include <gert_FontWrapper.h>
#include <decor/Bloods.h>
#include <decor/Gibblets.h>

#include <mechanical/HitCircle.h>
#include <mechanical/HurtCircle.h>

#include <mechanical/EnemyBase.h>
#include <mechanical/WaveManage.h>
#include <dads/DadBase.h>

#include <gundoms/Bullet.h>
#include <gundoms/Dom.h>
#include <gundoms/WepPickup.h>
#include <gundoms/Guns.h>

#define MAX_BLOOD 3000
#define MAX_GIBS 20

#define MAX_HURTCIRCLES 200
#define MAX_HITCIRCLES 200
#define MAX_BULLETS 180
#define MAX_PICKUPS 6
#ifdef __GERT_DEBUG__
    #define HOWBIG( n ) std::cout << #n << ": " << sizeof( n ) << std::endl;

    #define PICKUP_TIME 3.0
#else
    #define PICKUP_TIME 7.5
#endif

#define HIGHSCORES_FILE "highscores.dat"
#define MAX_HIGHSCORES 10


GameManager::GameManager( void )
{
    /*
    #ifdef __GERT_DEBUG__
    HOWBIG( Dom )
    HOWBIG( Bullet )
    HOWBIG( dads::DadBase )
    HOWBIG( GameManager )
    #endif // __GERT_DEBUG__
    */

    //ctor
    gameState = gmsMENU;
    _hitC = new HitCircle[ MAX_HITCIRCLES ];
    _hurtC = new HurtCircle[ MAX_HURTCIRCLES ];
    _activeHitC = 0;

    _thevent = new SDL_Event;
    _mouse = new SDL_Point( { 0, 0 } );
    _theMenu = new Menu;
    _theUpgradeMenu = new UpgradeMenu;

    _musical = Mix_LoadMUS( "assets/sound/sunth.mp3" );
    if( !_musical )
    {
        #ifdef __GERT_DEBUG__
        std::cout << "Error loading music file\n" << Mix_GetError() << std::endl;
        #endif // __GERT_DEBUG__
        gameState = gmsQUITTING;
    }
    else
    {
        Mix_PlayMusic( _musical, -1 );
        Mix_PauseMusic();
    }

    //PLAYER
    _bullets = new Bullet[ MAX_BULLETS ];
    theDom = new Dom;

    //DROPS
    _drops = new WepPickup[ MAX_PICKUPS ];
    _lastDrop = 0;

    //WAVE
    waver = new WaveManage;
    _inCombat = false;
    _bleed = new Bloods[ MAX_BLOOD ];
    _gib = new Gibblets[ MAX_GIBS ];
    _activeBleed = 0;

    //SCORES
    _score = 0;
    _highScores = new int[ MAX_HIGHSCORES ];
    _currentHighScore = -1;
    _exitHighScoreViewHeld = false;
    _clickExitHighScoreView = new SDL_Rect( { 0, 0, 0, 0 } );

    SDL_RWops* highscores = SDL_RWFromFile( HIGHSCORES_FILE, "r" );
    if( highscores != NULL )
    {
        for( int i = 0; i < MAX_HIGHSCORES; i++ )
        {
            SDL_RWread( highscores, &_highScores[ i ], 1, sizeof( int ) );
        }
    }
    else
    {
        highscores = SDL_RWFromFile( HIGHSCORES_FILE, "w" );
        for( int i = 0; i < MAX_HIGHSCORES; i++ )
        {
            _highScores[ i ] = MAX_HIGHSCORES - i;
            SDL_RWwrite( highscores, &i, sizeof( int ), 1 );
        }
    }
    SDL_RWclose( highscores );
}

GameManager::~GameManager( void )
{
    //dtor
    delete[] _hitC;
    delete[] _hurtC;
    delete[] _bullets;
    delete[] _drops;
    delete[] _highScores;
    delete[] _bleed;
    delete _thevent;
    delete _mouse;
    delete theDom;
    delete _theMenu;
    delete _theUpgradeMenu;
    delete waver;

    Mix_FreeMusic( _musical );
}

HurtCircle* GameManager::request_hurtCircle( EnemyBase* owner, int* phealth )
{
    for( int i = 0; i < MAX_HURTCIRCLES; i++ )
    {
        if( _hurtC[ i ].owner == nullptr )
        {
            _hurtC[ i ].owner = owner;
            _hurtC[ i ].set_pHealth( phealth );
            return &_hurtC[ i ];
        }
    }
    return nullptr;
}

void GameManager::pull_hurtCircles( EnemyBase* rm )
{
    for( int i = 0; i < MAX_HURTCIRCLES; i++ )
    {
        if( _hurtC[ i ].owner == rm )
        {
            _hurtC[ i ].owner = nullptr;
        }
    }
}

HitCircle* GameManager::set_hitCircle( Circle spot, bool dad, int damage )
{
    for( int i = 0; i < MAX_HITCIRCLES; i++ )
    {
        if( _hitC[ i ].alive == false )
        {
            _hitC[ i ].start( spot, dad, damage );
            if( i > _activeHitC - 1 ) _activeHitC = i + 1;
            return &_hitC[ i ];
        }
    }
    return nullptr;
}

Bullet* GameManager::set_bullet( SDL_Point spot, float angle, bool dad, int damage )
{
    for( int i = 0; i < MAX_BULLETS; i++ )
    {
        if( _bullets[ i ].get_dead() )
        {
            _bullets[ i ].start( spot, angle, dad, damage );
            return &_bullets[ i ];
            break;
        }
    }
    return nullptr;
}

void GameManager::set_blood( const SDL_Point& pos )
{
    if( _activeBleed < MAX_BLOOD )
    {
        for( int i = _activeBleed; i < MAX_BLOOD; i++ )
        {
            if( !_bleed[ i ].alive )
            {
                _bleed[ i ].start( pos );
                _activeBleed = i;
                break;
            }
        }
        if( _activeBleed == MAX_BLOOD - 1 )
        {
            _activeBleed = MAX_BLOOD;
        }
    }
    else
    {
        int fakeActive = _activeBleed;
        while( fakeActive >= MAX_BLOOD ) fakeActive -= MAX_BLOOD;

        if( fakeActive >= 0 )
        {
            _bleed[ fakeActive ].start( pos );
            _activeBleed += 1;
        }
    }
}

void GameManager::set_gib( const SDL_Point& pos )
{
    for( int i = 0; i < MAX_GIBS; i++ )
    {
        if( !_gib[ i ].alive )
        {
            _gib[ i ].start( pos );
            break;
        }
    }
}

#define SCORE_DECAY 5
#define SCORE_WAVE_BONUS 200

void GameManager::update( void )
{
    while( SDL_PollEvent( _thevent ) )
    {
        switch( gameState ) // gamestate req
        {
        case gmsPLAYING:
            {
                if( !_inCombat )
                {
                    theDom->upgrade( _theUpgradeMenu->interact( *_mouse, *_thevent ) );
                }
                switch( _thevent->type )
                {
                case SDL_KEYDOWN:
                    switch( _thevent->key.keysym.sym )
                    {
                    #ifdef __GERT_DEBUG__
                    case SDLK_1:
                        theDom->set_pickgun( gunTypes::SlowShot, 60 );
                        break;
                    case SDLK_2:
                        theDom->set_pickgun( gunTypes::Line, 20 );
                        break;
                    case SDLK_3:
                        theDom->set_pickgun( gunTypes::OddShot, 20 );
                        break;
                    case SDLK_4:
                        theDom->set_pickgun( gunTypes::TwoSpray, 40 );
                        break;
                    case SDLK_5:
                        theDom->set_pickgun( gunTypes::Nuke, 3 );
                        break;
    //                case SDLK_6:
    //                    theDom->set_pickgun( gunTypes::Sniper, 8 );
    //                    break;
                    #endif // __GERT_DEBUG__
                    case SDLK_t:
                        if( !_inCombat )
                        {
                            waver->start_wave();
                            _inCombat = true;
                            _score += SCORE_WAVE_BONUS;
                        }
                        break;
                    }
                    break;
                }
            }
            break;//END PLAYING INPUTS
        case gmsMENU:
            {
                switch( _theMenu->interact( *_thevent, *_mouse ) )
                {
                case Menu::mCONTINUE:
                    if( !theDom->is_dead() )
                    {
                        gameState = gmsPLAYING;
                        Mix_ResumeMusic();
                        break;
                    }
                case Menu::mNEW_GAME: //RESTART RESET GAME OVER
                    gameState = gmsPLAYING;
                    Mix_RewindMusic();
                    Mix_ResumeMusic();

                    waver->abort_wave();
                    _inCombat = false;
                    for( int i = 0; i < MAX_PICKUPS; i++ )
                    {
                        _drops[ i ].force_kill();
                        _drops[ i ].sated = true;
                    }
                    for( int i = 0; i < MAX_BULLETS; i++ )
                    {
                        _bullets[ i ].force_kill();
                    }
                    for( int i = 0; i < MAX_BLOOD && i < _activeBleed; i++ )
                    {
                        _bleed[ i ].alive = false;
                    }
                    for( int i = 0; i < MAX_GIBS; i++ )
                    {
                        _gib[ i ].alive = false;
                    }
                    _activeBleed = 0;
                    theDom->start( { 500, 400 } );

                    _score = 0;
                    break;
                case Menu::mHIGHSCORES:
                    gameState = gmsHIGHSCOREVIEW;
                    break;
                case Menu::mFULL_SCREEN:
                    _theMenu->fullscreenTemp = !_theMenu->fullscreenTemp;
                    _theMenu->restartReq = true;
                    _theMenu->resize_me();
                    break;
                case Menu::mWINDOWED_SIZE:
                    {
                        if( _theMenu->fullscreenTemp )
                        {
                            _theMenu->fullscreenTemp = false;
                        }

                        bool loopBack = true;
                        for( int i = 0; i < WINDOW_SIZES_TOTAL - 1; i++ )
                        {
                            if( gCamera.w == Menu::CLASSIC_WINDOW_SIZES[ i * 2 + 0 ] )
                            {
                                loopBack = false;
                                gCamera.w = Menu::CLASSIC_WINDOW_SIZES[ ( i + 1 )*2 + 0 ];
                                gCamera.h = Menu::CLASSIC_WINDOW_SIZES[ ( i + 1 )*2 + 1 ];
                                break;
                            }
                        }

                        if( loopBack )
                        {
                            gCamera.w = Menu::CLASSIC_WINDOW_SIZES[ 0 ];
                            gCamera.h = Menu::CLASSIC_WINDOW_SIZES[ 1 ];
                        }
                        _theMenu->restartReq = true;
                        _theMenu->resize_me();
                    }
                    break;
                case Menu::mVOLUME_CONTROL:
                    break;
                case Menu::mMUSIC_VOLUME:
                    break;
                case Menu::mQUIT:
                    gameState = gmsQUITTING;
                    break;
                case Menu::mTOTAL:
                    break;
                }
            }
            break; // END MENU INPUTS
        case gmsQUITTING:
            break;
        case gmsHIGHSCOREVIEW:
            switch( _thevent->type )
            {
            case SDL_MOUSEBUTTONDOWN:
                if( collision::get_collide( Circle{ float( _mouse->x ), float( _mouse->y ), 1 }, *_clickExitHighScoreView ) )
                {
                    _exitHighScoreViewHeld = true;
                }
                break;
            case SDL_MOUSEBUTTONUP:
                if( _exitHighScoreViewHeld && collision::get_collide( Circle{ float( _mouse->x ), float( _mouse->y ), 1 }, *_clickExitHighScoreView ) )
                {
                    gameState = gmsMENU;
                }
                _exitHighScoreViewHeld = false;
                break;
            }
        }//END GAMESTATE CHECK

        switch( _thevent->type )//GLOBAL INPUTS
        {
        case SDL_QUIT:
            gameState = gmsQUITTING;
            break;
        case SDL_KEYDOWN:
            switch( _thevent->key.keysym.sym )
            {
            case SDLK_F4:
                gameState = gmsQUITTING;
                break;
            case SDLK_ESCAPE:
                gameState = gmsMENU;
                Mix_PauseMusic();
                break;
            }
            break;//END KEYDOWN
        case SDL_MOUSEMOTION:
            SDL_GetMouseState( &_mouse->x, &_mouse->y );
            break;
        }//END GLOBAL INPUTS
        theDom->interactive( *_thevent, *_mouse );
    }//END POLL EVENTS

    switch( gameState )
    {
    case gmsPLAYING:
        if( _inCombat )
        {
            _lastDrop += gFrameTime;
            _score -= gFrameTime * SCORE_DECAY;
        }
        else
        {
            _lastDrop = 0;
        }

        for( int i = 0; i < MAX_GIBS; i++ )
        {
            _gib[ i ].update();
        }

        for( int i = 0; i < MAX_PICKUPS; i++ )
        {
            if( _drops[ i ].is_dead() )
            {
                if( !_drops[ i ].sated )
                {
                    theDom->set_pickgun( _drops[ i ].wep, _drops[ i ].ammo );
                    _drops[ i ].sated = true;
                    _score += 20;
                }
                if( _lastDrop > PICKUP_TIME )
                {
                    _drops[ i ].start( { rand()%gCamera.w + gCamera.x, rand()%gCamera.h + gCamera.y } );
                    _lastDrop -= PICKUP_TIME;
                }
            }//end dead drops?
            _drops[ i ].update();
        }

        waver->update();

        for( int i = 0; i < MAX_BULLETS; i++ )
        {
            _bullets[ i ].update();
        }
        theDom->update();

        //COLLISION DETECT
        for( int i = 0; i < _activeHitC; i++ )
        {
            if( _hitC[ i ].alive )
            {
                for( int u = 0; u < MAX_HURTCIRCLES; u++ )
                {
                    if( _hurtC[ u ].owner != nullptr )
                    {
                        _hurtC[ u ].check_collide( &_hitC[ i ] );
                    }
                }
            }
            _hitC[ i ].alive = false;
        }

        //KILL CHECK
        if( _inCombat && waver->wave_over() )
        {
            _inCombat = false;
            _score += waver->get_totalDads() * 10 - SCORE_WAVE_BONUS;
        }
        if( theDom->is_dead() )
        {
            gameState = gmsMENU;
            Mix_PauseMusic();
            _push_highscore();
            gSounds[ TexHolder::sPLAYERDEATH ].play();
        }
        break;
    case gmsMENU:
        break;
    case gmsHIGHSCOREVIEW:
        break;
    case gmsQUITTING:
        _write_highscores();
        _theMenu->save_settings();
        break;
    }//GAMESTATE SPECIFIC UPDATES
}

void GameManager::draw( void )
{
    SDL_SetRenderDrawColor( gRenderer, 255, 255, 255, 255 );
    SDL_RenderClear( gRenderer );

    if( gameState != gmsQUITTING )
    {
        gTextures[ TexHolder::tBACKGROUND ].render( gCamera, color::WHITE );
        for( int i = 0; i < MAX_BLOOD && i <= _activeBleed; i++ )
        {
            _bleed[ i ].draw();
        }
        waver->draw();
        for( int i = 0; i < MAX_GIBS; i++ )
        {
            _gib[ i ].draw();
        }
        for( int i = 0; i < MAX_PICKUPS; i++ )
        {
            _drops[ i ].draw();
        }
        for( int i = 0; i < MAX_BULLETS; i++ )
        {
            _bullets[ i ].draw();
        }
        theDom->draw();

        std::stringstream scorespot; scorespot << "Score: " << int( _score );
        SDL_Rect scorener = { 0, 0, gFont->string_width( scorespot.str() ), 2 };
        gFont->render( scorener, ct_string( scorespot.str() ), color::MAGENTA );

        if( gameState == gmsPLAYING )
        {
            if( !_inCombat )
            {
                #define str_START "Press 'T' to start"
                SDL_Rect lin1 = rectAlign::middle( { 0, 0, gFont->string_width( str_START ), 2 }, gCamera );
                gFont->render( lin1, str_START, color::BLACK );

                _theUpgradeMenu->draw();
            }
            std::stringstream ss; ss << "Dads: " << waver->get_dadsLeft();
            SDL_Rect lin1 = rectAlign::middle( { 0, FONT_H, gFont->string_width( ss.str() ), 2 }, gCamera );
            gFont->render( lin1, ss.str(), color::RED );
        }//END NO MENU
    }
    if( gameState == gmsMENU )
    {
        _theMenu->draw();
    }
    if( gameState == gmsHIGHSCOREVIEW )
    {
        SDL_Rect readout = rectAlign::middle( { 0, 0, 200, 40 }, gCamera );
        for( int i = 0; i < MAX_HIGHSCORES; i++ )
        {
            SDL_Color c = color::BLACK;
            if( _currentHighScore == i )
            {
                c = color::CYAN;
            }
            readout.w = gFont->string_width( ct_string( _highScores[ i ] ) );
            gFont->render( readout, ct_string( _highScores[ i ] ), c );
            readout.y += FONT_H + 2;
        }

        *_clickExitHighScoreView = { readout.x, readout.y, gFont->string_width( "EXIT" ), FONT_H };
        SDL_Color c = color::WHITE;
        if( _exitHighScoreViewHeld )
        {
            c = color::CYAN;
        }
        gTextures[ TexHolder::tBULLET ].render( *_clickExitHighScoreView, c );
        gFont->render( *_clickExitHighScoreView, "EXIT", color::BLACK );
    }

    #ifdef __GERT_DEBUG__
    for( int i = 0; i < MAX_HURTCIRCLES; i++ )
    {
        if( _hurtC[ i ].owner != nullptr )
        {
            gTextures[ TexHolder::tCIRCLE ].render( _hurtC[ i ].area->ct_Rect(), { 255, 255, 0, 120 } );
        }
    }
    gFont->render( rectAlign::right( { 0, 0, gFont->string_width( "Debug on" ), 9 }, gCamera ), "Debug on", color::MAGENTA );
    #endif // __GERT_DEBUG__

    SDL_RenderPresent( gRenderer );
}

void GameManager::_push_highscore( void )
{
    int core = int( _score );
    for( int i = 0; i < MAX_HIGHSCORES; i++ )
    {
        if( core > _highScores[ i ] )
        {
            for( int a = MAX_HIGHSCORES - 1; a > i ; a-- )
            {
                _highScores[ a ] = _highScores [ a - 1 ];
            }
            _highScores[ i ] = core;
            _currentHighScore = i;
            break;
        }
        _currentHighScore = -1;
    }
}

void GameManager::_write_highscores( void )
{
    SDL_RWops* hs = SDL_RWFromFile( HIGHSCORES_FILE, "w" );
    if( hs != NULL )
    {
        for( int i = 0; i < MAX_HIGHSCORES; i++ )
        {
            SDL_RWwrite( hs, &_highScores[ i ], sizeof( int ), 1 );
        }
        SDL_RWclose( hs );
    }
}
