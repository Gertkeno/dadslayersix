#include "mechanical/WaveManage.h"

#include <SDL2/SDL.h>
#include <mechanical/GameManager.h>
#include <gert_Collision.h>
#include <random>

//THE DAD TEAM
#include <dads/DadBase.h>
#include <dads/Grunt.h>
#include <dads/BigDad.h>
#include <dads/EngiDad.h>
#include <dads/GigaDad.h>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

#define STARTING_DADS 30

WaveManage::WaveManage( void )
{
    //ctor
    _totalDads = STARTING_DADS;
    _myDads = nullptr;
}

WaveManage::~WaveManage( void )
{
    //dtor
    if( _myDads != nullptr )
    {
        delete[] _myDads;
    }
}

void WaveManage::start_wave( void )
{
    if( _myDads == nullptr )
    {
        _ellapsedTime = 0;
        _revvedDads = -2;

        #ifdef __GERT_DEBUG__
        int dadtypes[ dads::dTOTAL ];
        for( int i = 0; i < dads::dTOTAL; i++ ) dadtypes[ i ] = 0;
        #endif // __GERT_DEBUG__

        _myDads = new dads::DadBase*[ _totalDads ];
        for( int i = 0; i < _totalDads; i++ )
        {
            /* Setting the percents as 1.0 - x.0/y makes it easier to recognize how many of each type there will be
            compared to grunts, like a GigaDad only spawns 1 for every 95 total dads and then grunts fill the rest. */
            if( float( i )/float( _totalDads ) >= 1.0 - 1.0/95 )
            //#endif // __GERT_DEBUG__
            {
                _myDads[ i ] = new dads::GigaDad;
                #ifdef __GERT_DEBUG__
                dadtypes[ dads::dGIGADAD ] += 1;
                #endif // __GERT_DEBUG__
            }
            else if( float( i )/float( _totalDads ) >= 1.0 - 3.0/40 )
            {
                _myDads[ i ] = new dads::BigDad;
                #ifdef __GERT_DEBUG__
                dadtypes[ dads::dBIGDAD ] += 1;
                #endif // __GERT_DEBUG__
            }
            else if( float( i ) / float( _totalDads ) >= 1.0 - 11.0/30 )
            {
                _myDads[ i ] = new dads::EngiDad;
                #ifdef __GERT_DEBUG__
                dadtypes[ dads::dENGIDAD ] += 1;
                #endif // __GERT_DEBUG__
            }
            else
            {
                _myDads[ i ] = new dads::Grunt;
                #ifdef __GERT_DEBUG__
                dadtypes[ dads::dGRUNT ] += 1;
                #endif // __GERT_DEBUG__
            }
        }
        _shuffle_dads();

        #ifdef __GERT_DEBUG__
        std::cout << "started wave with total " << _totalDads << "\nDad type breakdown inc.." << std::endl;
        for( int i = 0; i < dads::dTOTAL; i++ )
        {
            std::cout << i << "-\t" << dadtypes[ i ] << std::endl;
        }
        std::cout << std::endl;
        #endif // __GERT_DEBUG__
    }
    #ifdef __GERT_DEBUG__
    else
    {
        std::cout << "Wave is not set to nullptr!\n";
    }
    #endif // __GERT_DEBUG__
}

bool WaveManage::wave_over( void )
{
    bool noDadsLeft = true;
    if( _myDads != nullptr )
    {
        for( int i = 0; i < _totalDads; i++ )
        {
            if( _myDads[ i ] != nullptr )
            {
                if( !_myDads[ i ]->is_dead() )
                {
                    noDadsLeft = false;
                    break;
                }
            }
        }
    }

    if( noDadsLeft )
    {
        for( int i = 0; i < _totalDads; i++ )
        {
            gGameManager->pull_hurtCircles( _myDads[ i ] );
            _myDads[ i ]->force_kill();
            delete _myDads[ i ];
        }
        delete[] _myDads;
        _myDads = nullptr;

        _totalDads += 10 + _totalDads * 0.1;
        #ifdef __GERT_DEBUG__
        std::cout << "the wave has ENDED, elapsed time: " << _ellapsedTime << "\t new total dads: " << _totalDads << std::endl;
        #endif // __GERT_DEBUG__
    }

    return noDadsLeft;
}

void WaveManage::abort_wave( void )
{
    if( _myDads != nullptr )
    {
        for( int i = 0; i < _totalDads; i++ )
        {
            _myDads[ i ]->force_kill();
            gGameManager->pull_hurtCircles( _myDads[ i ] );
            delete _myDads[ i ];
        }
        delete[] _myDads;
        _myDads = nullptr;
    }
    _totalDads = STARTING_DADS;
}

#define START_ALL_BY 20.0

void WaveManage::update( void )
{
    if( _myDads != nullptr )
    {
        _ellapsedTime += gFrameTime;

        for( int i = 0; i < _totalDads; i++ )
        {
            if( _revvedDads < i && float( i )/float( _totalDads ) <= _ellapsedTime/START_ALL_BY )
            {
                #ifdef __GERT_DEBUG__
                //std::cout << "Comparing " << float( i )/float( _totalDads ) << " to " << _ellapsedTime/START_ALL_BY << "\t" << _revvedDads << "/" << i << std::endl;
                #endif // __GERT_DEBUG__
                _myDads[ i ]->start( { rand()%gCamera.w + gCamera.x, gCamera.y - 80 } );
                _revvedDads = i;
            }
        }

        for( int i = 0; i < _totalDads; i++ )
        {
            _myDads[ i ]->update();
        }
    }
}

void WaveManage::draw( void )
{
    if( _myDads != nullptr )
    {
        for( int i = 0; i < _totalDads; i++ )
        {
            _myDads[ i ]->draw();
        }
    }
}

int WaveManage::apply_inRange( dads::DadBase* source, const Circle& area, void (*apply)( dads::DadBase* t ) )
{
    int totalHit = 0;
    if( _myDads != nullptr )
    {
        for( int i = 0; i < _totalDads; i++ )
        {
            if( !_myDads[ i ]->is_dead() && _myDads[ i ]->get_revved() )
            {
                Circle foo = { float( _myDads[ i ]->get_syncPoint().x ), float( _myDads[ i ]->get_syncPoint().y ), 5 };
                if( source != _myDads[ i ] && collision::get_collide( foo, area ) )
                {
                    if( apply != NULL )
                    {
                        apply( _myDads[ i ] );
                    }
                    totalHit += 1;
                }
            }
        }
    }
    return totalHit;
}

int WaveManage::get_dadsLeft( void )
{
    if( _myDads != nullptr )
    {
        int leftones = 0;
        for( int i = 0; i < _totalDads; i++ )
        {
            if( !_myDads[ i ]->is_dead() )
            {
                leftones += 1;
            }
        }
        return leftones;
    }
    return _totalDads;
}

/* This function is pretty self explained and is technically a sorting algorithm. Since _myDads is an array of pointers this funciton is very easy */
void WaveManage::_shuffle_dads( void )
{
    if( _myDads != nullptr )
    {
        for( int i = _totalDads - 1; i > 0; --i )
        {
            dads::DadBase* temp = _myDads[ i ];
            int rng = rand()% i + 1;
            _myDads[ i ] = _myDads[ rng ];
            _myDads[ rng ] = temp;
        }
    }
}
