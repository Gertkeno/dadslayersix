#include "Turret.h"

#include <gundoms/Guns.h>
#include <mechanical/GameManager.h>
#include <mechanical/HurtCircle.h>
#include <gert_Collision.h>

#include <mechanical/TexHolder.h>
#include <gert_Wrappers.h>
#include <gert_SoundWrapper.h>

#define TURRET_SIZE 20
#define TURRET_DRAW_SIZE 25
#define TURRET_TOTAL_CD ( _cooldown * 3 )

Turret::Turret( void )
{
    //ctor
    _hurtCircles = 1;
    _myHurtCircles = new HurtCircle*;
    *_myHurtCircles = nullptr;

    mGun = gunTypes::BasicPellet;
    _health = 0;
    _dad = true;

    _lastShot = 0.0;
    angle = 0;
    _cooldown = 0.0;
}

Turret::~Turret( void )
{
    //dtor
    delete _myHurtCircles;
}

void Turret::update( void )
{
    if( !is_dead() && gGameManager->get_inCombat() )
    {
        _lastShot += gFrameTime;
        if( _lastShot > TURRET_TOTAL_CD )
        {
            mGun( *_syncPoint, angle, &_cooldown, _dad );
            gSounds[ TexHolder::sPEW ].play();
            _lastShot = 0.0;
        }
        _sync_all();
    }
    else if( is_dead() )
    {
        _pull_hurtCircles();
    }
}

void Turret::draw( void )
{
    if( !is_dead() )
    {
        int frameN = _lastShot / TURRET_TOTAL_CD * gTextures[ TexHolder::tTURRET ].get_maxFrames();
        gTextures[ TexHolder::tTURRET ].render( get_circ_from_point( TURRET_DRAW_SIZE ).ct_Rect(), color::WHITE, frameN, angle );
    }
    Actor::draw();
}

void Turret::start( const SDL_Point& pos )
{
    Actor::start( pos );
    _pull_hurtCircles();
    *_myHurtCircles = gGameManager->request_hurtCircle( this, &_health );
    _sync_all();

    _health = 3;
    _lastShot = 0.0;
    _cooldown = 0.7;
}

void Turret::_sync_all( void )
{
    if( *_myHurtCircles != nullptr )
    {
        *(*_myHurtCircles)->area = get_circ_from_point( TURRET_SIZE );
    }
}
