#include "dads/EngiDad.h"

#include <mechanical/GameManager.h>
#include <Turret.h>
#include <SDL2/SDL.h>
#include <mechanical/HitCircle.h>
#include <mechanical/HurtCircle.h>
#include <mechanical/TexHolder.h>
#include <gert_Wrappers.h>
#include <gert_Collision.h>

#include <gundoms/Dom.h>
#include <gert_CameraMath.h>

#define RADIUS 25
#define MAX_TURRET_RANGE 400

namespace dads
{
    EngiDad::EngiDad( void )
    {
        //ctor
        _myTurret = new Turret;
        _hurtCircles = 1;
        _myHurtCircles = new HurtCircle*;
        *_myHurtCircles = nullptr;
    }

    EngiDad::~EngiDad( void )
    {
        //dtor
        delete _myTurret;
        delete _myHurtCircles;
    }

    void EngiDad::update( void )
    {
        DadBase::update();
        if( !is_dead() && _revved )
        {
            if( !_myTurret->is_dead() && collision::distance( get_syncPoint(), _myTurret->get_syncPoint() ) > MAX_TURRET_RANGE )
            {
                _myTurret->force_kill();
            }

            float diffy = 0;
            collision::get_normal_diffXY( *_syncPoint, gGameManager->theDom->get_syncPoint(), NULL, &diffy );
            if( _myTurret->is_dead() && diffy <= -0.98 )
            {
                _myTurret->start( *_syncPoint );
                _myTurret->angle = 90;
            }

            if( _lastAttack > attackSpeed )
            {
                HitCircle* h = gGameManager->set_hitCircle( get_circ_from_point( attackRange ), true, 1 );
                if( h != nullptr )
                {
                    h->didCollide = &_hit;
                }
                _lastAttack -= attackSpeed;
            }

            _myTurret->update();
            _sync_all();
        }
        else
        {
            _myTurret->force_kill();
        }
    }

    #define WIRE_WIDTH 8
    void EngiDad::draw( void )
    {
        if( !is_dead() && _revved )
        {
            if( !_myTurret->is_dead() )
            {
                float wAngle = angles::get_angle( *_syncPoint, _myTurret->get_syncPoint() );
                int distance = ( collision::distance( *_syncPoint, _myTurret->get_syncPoint() ) / ( WIRE_WIDTH *  2 ) + 0.5 );
                Circle wire[ distance ];

                float diffX, diffY;
                collision::get_normal_diffXY( *_syncPoint, _myTurret->get_syncPoint(), &diffX, &diffY );
                for( int i = 0; i < distance; i++ )
                {
                    wire[ i ] = { _myTurret->get_syncPoint().x + diffX * i * WIRE_WIDTH * 2, _myTurret->get_syncPoint().y + diffY * i * WIRE_WIDTH * 2, WIRE_WIDTH };
                    gTextures[ TexHolder::tWIRE ].render( wire[ i ].ct_Rect(), color::WHITE, 0, wAngle );
                }
            }

            #define IW 40
            #define IH 60
            gTextures[ TexHolder::tDAD_ENGI ].render( SDL_Rect{ _syncPoint->x - IW/2, _syncPoint->y - IH/2, IW, IH }, *_myColor );

            _myTurret->draw();
        }
        DadBase::draw();
    }

    void EngiDad::start( const SDL_Point& pos )
    {
        DadBase::start( pos );
        *_myHurtCircles = gGameManager->request_hurtCircle( this, &_health );
        _sync_all();

        _health = 7;
    }

    void EngiDad::_sync_all( void )
    {
        if( *_myHurtCircles != nullptr )
        {
            *(*_myHurtCircles)->area = get_circ_from_point( RADIUS );
        }
    }

    void EngiDad::force_kill( int nh )
    {
        EnemyBase::force_kill( nh );
        _myTurret->force_kill( nh );
        /* checks collision and sets health before engidad can update again
            in that update engidad would've done this so we force kill on wave end*/
    }
}
