#include "dads/DadBase.h"

#include <mechanical/GameManager.h>
#include <mechanical/WaveManage.h>
#include <mechanical/TexHolder.h>
#include <gert_Collision.h>
#include <gert_SoundWrapper.h>
#include <SDL2/SDL.h>

#ifdef __GERT_DEBUG__
    #include <iostream>
    #include <gert_Wrappers.h>
#endif // __GERT_DEBUG__

#include <gundoms/Dom.h>

namespace dads
{
    DadBase::DadBase( void )
    {
        //ctor
        _dad = true;
        _health = 1;
        _lastAttack = 0;
        _revved = false;
        _dead = false;
        _hurtCircles = 0;
        _myColor = new SDL_Color( { Uint8( rand()%255 ), Uint8( rand()%255 ), Uint8( rand()%255 ), 255 } );

        //stats
        attackSpeed = 4;
        attackRange = 10;
        movementSpeed = 10;
        _hit = false;
    }

    DadBase::~DadBase( void )
    {
        //dtor
        delete _myColor;
    }

    void DadBase::update( void )
    {
        if( !is_dead() && _revved )
        {
            float diffX = 0, diffY = 0;
            collision::get_normal_diffXY( *_syncPoint, gGameManager->theDom->get_syncPoint(), &diffX, &diffY );

            _xBuffer -= diffX * movementSpeed * gFrameTime;
            _yBuffer -= diffY * movementSpeed * gFrameTime;
            _syncPoint->x = _xBuffer;
            _syncPoint->y = _yBuffer;

            _lastAttack += gFrameTime;
        }
        else
        {
            if( !_dead && _revved )
            {
                _dead = true;
                gGameManager->set_blood( *_syncPoint );
                if( rand() % 7 == 0 )
                {
                    gSounds[ ( rand() % ( TexHolder::s2DAD_SCREAM - TexHolder::s0DAD_SCREAM + 1 ) ) + TexHolder::s0DAD_SCREAM ].play(); // needs to be a shorter sound
                }
            }
            _pull_hurtCircles();
        }

        if( _hit )
        {
            gSounds[ TexHolder::s0DAD_HIT ].play();
            _hit = false;
        }
    }

    void DadBase::draw( void )
    {
        Actor::draw();
        #ifdef __GERT_DEBUG__
        if( !is_dead() )
        {
            gTextures[ TexHolder::tCIRCLE ].render( get_circ_from_point( attackRange ).ct_Rect(), { 255, 0, 0, 30 } );
        }
        #endif // __GERT_DEBUG__
    }

    void DadBase::start( const SDL_Point& pos )
    {
        Actor::start( pos );
        _xBuffer = _syncPoint->x;
        _yBuffer = _syncPoint->y;
        _revved = true;
        _dead = false;

        _lastAttack = 0;
        attackSpeed = base_attackSpeed();
        movementSpeed = base_movementSpeed();
        attackRange = base_attackRange();
        _pull_hurtCircles();

        _hit = false;

        if( rand()%6 == 0 )
        {
            gSounds[ rand() % TexHolder::s0DAD_HIT ].play();
        }
    }
}
