#include "dads/GigaDad.h"

#include <mechanical/HurtCircle.h>
#include <mechanical/TexHolder.h>
#include <mechanical/GameManager.h>
#include <mechanical/HitCircle.h>

#include <gert_Collision.h>
#include <gert_Wrappers.h>

#define HAND_RADIUS 30
#define HEAD_RADIUS 40
#define DAMAGE 6

#define ANIMATION_ATTACK_TIME 0.2
#define ANIMATION_DELAY ( base_attackSpeed()/3 )

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

enum healthguide
{
    hgLEFT,
    hgRIGHT,
    hgHEAD,
    hgTOTAL
};

namespace dads
{
    GigaDad::GigaDad( void )
    {
        //ctor
        _hurtCircles = hgTOTAL;
        _myHurtCircles = new HurtCircle*[ hgTOTAL ];
        for( int i = 0; i < hgTOTAL; i++ ){ _myHurtCircles[ i ] = nullptr; }
        _leftHand = 0;
        _rightHand = 0;
        _head = 0;
        _bodyPoints = new SDL_Point[ hgTOTAL ];
    }

    GigaDad::~GigaDad( void )
    {
        //dtor
        delete[] _myHurtCircles;
        delete[] _bodyPoints;
    }

    void GigaDad::update( void )
    {
        DadBase::update();
        if( !is_dead() && _revved )
        {
            bool procAttack = false;
            if( _lastAttack > attackSpeed )
            {
                procAttack = true;
                _lastAttack -= attackSpeed;
            }
            if( _myHurtCircles[ hgLEFT ] != nullptr )
            {
                if( _leftHand < 1 )
                {
                    _myHurtCircles[ hgLEFT ]->owner = nullptr;
                    _myHurtCircles[ hgLEFT ]->set_pHealth( nullptr );
                    _myHurtCircles[ hgLEFT ] = nullptr;
                    _health -= 1;
                    gGameManager->set_blood( _bodyPoints[ hgLEFT ] );
                }
                else
                {
                    if( procAttack )
                    {
                        HitCircle* h = gGameManager->set_hitCircle( { float( _bodyPoints[ hgLEFT ].x ), float( _bodyPoints[ hgLEFT ].y ), attackRange }, _dad, DAMAGE );
                        if( h != nullptr )
                        {
                            h->didCollide = &_hit;
                        }
                    }
                }
            }
            if( _myHurtCircles[ hgRIGHT ] != nullptr )
            {
                if( _rightHand < 1 )
                {
                    _myHurtCircles[ hgRIGHT ]->owner = nullptr;
                    _myHurtCircles[ hgRIGHT ]->set_pHealth( nullptr );
                    _myHurtCircles[ hgRIGHT ] = nullptr;
                    _health -= 1;
                    gGameManager->set_blood( _bodyPoints[ hgRIGHT ] );
                }
                else
                {
                    if( procAttack )
                    {
                        HitCircle* h = gGameManager->set_hitCircle( { float( _bodyPoints[ hgRIGHT ].x ), float( _bodyPoints[ hgRIGHT ].y ), attackRange }, _dad, DAMAGE );
                        if( h != nullptr )
                        {
                            h->didCollide = &_hit;
                        }
                    }
                }
            }
            if( _myHurtCircles[ hgHEAD ] != nullptr )
            {
                if( _head < 1 )
                {
                    _myHurtCircles[ hgHEAD ]->owner = nullptr;
                    _myHurtCircles[ hgHEAD ]->set_pHealth( nullptr );
                    _myHurtCircles[ hgHEAD ] = nullptr;
                    _health -= 2;
                    gGameManager->set_blood( _bodyPoints[ hgHEAD ] );
                }
            }

            if( _health < 2 )
            {
                movementSpeed = base_movementSpeed() * 1.3;
            }
            else
            {
                movementSpeed = base_movementSpeed();
            }

            _sync_all();
        }
    }

    #define ANIMATION_FRAME_MAX ( gTextures[ TexHolder::tGIGA_HAND ].get_maxFrames() - 1 )
    #define HAND_WIDTH 60
    #define HAND_HEIGHT 80
    void GigaDad::draw( void )
    {
        if( !is_dead() && _revved )
        {
            Circle AARange = { 0, 0, 0 };
            SDL_Color AAAlpha = { 255, 0, 0, 255 };
            int AAFrame = ( _lastAttack / ANIMATION_ATTACK_TIME ) * ANIMATION_FRAME_MAX;
            if( AAFrame > ANIMATION_FRAME_MAX )
            {
                AAFrame = ANIMATION_FRAME_MAX;
            }

            AARange.r = float( _lastAttack / ( base_attackSpeed()/3 ) * base_attackRange() );
            if( AARange.r > base_attackRange() ) AARange.r = base_attackRange();
            AAAlpha.a = Uint8( ( base_attackSpeed() - _lastAttack ) / base_attackSpeed() * 254 );

            if( _myHurtCircles[ hgHEAD ] != nullptr )
            {
                SDL_Rect foo = { _bodyPoints[ hgHEAD ].x - HEAD_RADIUS, _bodyPoints[ hgHEAD ].y - HEAD_RADIUS, HEAD_RADIUS*2, HEAD_RADIUS*2 };
                gTextures[ TexHolder::tGIGA_HEAD ].render( foo, *_myColor );
            }


            if( _myHurtCircles[ hgLEFT ] != nullptr )
            {
                AARange.x = _bodyPoints[ hgLEFT ].x; AARange.y = _bodyPoints[ hgLEFT ].y;
                gTextures[ TexHolder::tRING ].render( AARange.ct_Rect(), AAAlpha );

                SDL_Rect foo = { _bodyPoints[ hgLEFT ].x - HAND_WIDTH/2, _bodyPoints[ hgLEFT ].y - HAND_HEIGHT/2, HAND_WIDTH, HAND_HEIGHT };
                gTextures[ TexHolder::tGIGA_HAND ].render( foo, *_myColor, AAFrame );

                #ifdef __GERT_DEBUG__
                SDL_Rect s;
                s.x = _bodyPoints[ hgLEFT ].x - base_attackRange();
                s.y = _bodyPoints[ hgLEFT ].y - base_attackRange();
                s.w = s.h = base_attackRange()*2;
                gTextures[ TexHolder::tCIRCLE ].render( s, { 255, 0, 0, 30 } );
                #endif // __GERT_DEBUG__
            }

            if( _myHurtCircles[ hgRIGHT ] != nullptr )
            {
                AARange.x = _bodyPoints[ hgRIGHT ].x; AARange.y = _bodyPoints[ hgRIGHT ].y;
                gTextures[ TexHolder::tRING ].render( AARange.ct_Rect(), AAAlpha );

                SDL_Rect foo = { _bodyPoints[ hgRIGHT ].x - HAND_WIDTH/2, _bodyPoints[ hgRIGHT ].y - HAND_HEIGHT/2, HAND_WIDTH, HAND_HEIGHT };
                gTextures[ TexHolder::tGIGA_HAND ].render( foo, *_myColor, AAFrame, 0, SDL_FLIP_HORIZONTAL );

                #ifdef __GERT_DEBUG__
                SDL_Rect s;
                s.x = _bodyPoints[ hgRIGHT ].x - base_attackRange();
                s.y = _bodyPoints[ hgRIGHT ].y - base_attackRange();
                s.w = s.h = base_attackRange()*2;
                gTextures[ TexHolder::tCIRCLE ].render( s, { 255, 0, 0, 30 } );
                #endif // __GERT_DEBUG__
            }
        }
        Actor::draw();
    }

    void GigaDad::start( const SDL_Point& pos )
    {
        DadBase::start( pos );
        _leftHand = 50;
        _rightHand = 50;
        _head = 80;
        _myHurtCircles[ hgLEFT ] = gGameManager->request_hurtCircle( this, &_leftHand );
        _myHurtCircles[ hgRIGHT ] = gGameManager->request_hurtCircle( this, &_rightHand );
        _myHurtCircles[ hgHEAD ] = gGameManager->request_hurtCircle( this, &_head );

        _health = 2;
    }

    /* This function is a life saver, having a syncing point is never a waste of memory */
    void GigaDad::_sync_all( void )
    {
        _bodyPoints[ hgLEFT ] = { _syncPoint->x - HAND_RADIUS*2, _syncPoint->y };
        _bodyPoints[ hgRIGHT ] = { _syncPoint->x + HAND_RADIUS*2, _syncPoint->y };
        _bodyPoints[ hgHEAD ] = { _syncPoint->x, _syncPoint->y - int( HAND_RADIUS*1.7 ) }; //*_syncPoint;
        if( _myHurtCircles[ hgLEFT ] != nullptr )
        {
            *(_myHurtCircles[ hgLEFT ])->area = { float( _bodyPoints[ hgLEFT ].x ), float( _bodyPoints[ hgLEFT ].y ), HAND_RADIUS };
        }

        if( _myHurtCircles[ hgRIGHT ] != nullptr )
        {
            *(_myHurtCircles[ hgRIGHT ])->area = { float( _bodyPoints[ hgRIGHT ].x ), float( _bodyPoints[ hgRIGHT ].y ), HAND_RADIUS };
        }

        if( _myHurtCircles[ hgHEAD ] != nullptr )
        {
            *(_myHurtCircles[ hgHEAD ])->area = { float( _bodyPoints[ hgHEAD ].x ), float( _bodyPoints[ hgHEAD ].y ),HEAD_RADIUS };
        }
    }
}
