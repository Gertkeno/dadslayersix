#include "dads/BigDad.h"

#include <mechanical/GameManager.h>
#include <mechanical/WaveManage.h>

#include <SDL2/SDL.h>
#include <mechanical/HurtCircle.h>
#include <mechanical/TexHolder.h>
#include <mechanical/HitCircle.h>
#include <gert_Wrappers.h>
#include <gert_Collision.h>

#define RADIUS 40

namespace dads
{
    BigDad::BigDad( void )
    {
        //ctor
        _hurtCircles = 1;
        _myHurtCircles = new HurtCircle*;
        *_myHurtCircles = nullptr;
    }

    BigDad::~BigDad( void )
    {
        //dtor
        delete _myHurtCircles;
    }

    void BigDad::draw( void )
    {
        if( !is_dead() && _revved )
        {
            int w = 60, h = 80;
            SDL_Rect foo = { _syncPoint->x - w/2, _syncPoint->y - h/2, w, h };
            gTextures[ TexHolder::tDAD_GRUNT ].render( foo, *_myColor );

            #ifdef __GERT_DEBUG__
            Circle dRange = get_circ_from_point( attackRange * 3 );
            gTextures[ TexHolder::tCIRCLE ].render( dRange.ct_Rect(), { 0, 255, 0, 30 } );
            #endif // __GERT_DEBUG__
        }
        DadBase::draw();
    }

    void BigDad::update( void )
    {
        DadBase::update();
        if( !is_dead() && _revved )
        {
            if( _lastAttack > attackSpeed )
            {
                HitCircle* h = gGameManager->set_hitCircle( get_circ_from_point( attackRange ), true, 3 );
                if( h != nullptr )
                {
                    h->didCollide = &_hit;
                }

                gGameManager->waver->apply_inRange( this, get_circ_from_point( attackRange * 3 ), BigDad::_buffSpeed );
                _lastAttack -= attackSpeed;
            }
            _sync_all();
        }
    }

    void BigDad::start( const SDL_Point& pos )
    {
        DadBase::start( pos );
        *_myHurtCircles = gGameManager->request_hurtCircle( this, &_health );

        _health = 16;
        _sync_all();
    }

    void BigDad::_sync_all( void )
    {
        if( *_myHurtCircles != nullptr )
        {
            *(*_myHurtCircles)->area = get_circ_from_point( RADIUS );
        }
    }

    void BigDad::_buffSpeed(DadBase* t)
    {
            t->movementSpeed = t->base_movementSpeed() + 90;
    }
}
