#include "dads/Grunt.h"
#include <SDL2/SDL.h>

#include <mechanical/GameManager.h>
#include <mechanical/TexHolder.h>
#include <mechanical/HitCircle.h>
#include <mechanical/HurtCircle.h>

#include <gert_Collision.h>
#include <gert_Wrappers.h>

#define RADIUS 25

namespace dads
{
    Grunt::Grunt( void )
    {
        //ctor
        _hurtCircles = 1;
        _myHurtCircles = new HurtCircle*;
        *_myHurtCircles = nullptr;
    }

    Grunt::~Grunt( void )
    {
        //dtor
        delete _myHurtCircles;
    }

    void Grunt::start( const SDL_Point& pos )
    {
        DadBase::start( pos );
        *_myHurtCircles = gGameManager->request_hurtCircle( this, &_health );
        _sync_all();

        //stats
        _health = 5;
    }

    void Grunt::update( void )
    {
        DadBase::update();
        if( !is_dead() && _revved )
        {
            if( _lastAttack > attackSpeed )
            {
                HitCircle* h = gGameManager->set_hitCircle( get_circ_from_point( attackRange ), true, 2 );
                if( h != nullptr )
                {
                    h->didCollide = &_hit;
                }
                _lastAttack -= attackSpeed;
            }
            _sync_all();
        }
    }

    void Grunt::draw( void )
    {
        if( !is_dead() && _revved )
        {
            int w = 40, h = 60;
            SDL_Rect pos = { _syncPoint->x - w/2, _syncPoint->y - h/2, w, h };
            gTextures[ TexHolder::tDAD_GRUNT ].render( pos, *_myColor );
        }
        DadBase::draw();
    }

    void Grunt::_sync_all( void )
    {
        if( *_myHurtCircles != nullptr )
        {
            *(*_myHurtCircles)->area = get_circ_from_point( RADIUS );
        }
    }
}
