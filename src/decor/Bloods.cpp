#include "decor/Bloods.h"

#include <gert_Wrappers.h>
#include <gert_Collision.h>
#include <mechanical/TexHolder.h>
#include <random>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

Bloods::Bloods( void )
{
    //ctor
    alive = false;
    _angle = ( rand()%36000 ) / 100.0;
    _tile = rand() % gTextures[ TexHolder::tBLOOD ].get_maxFrames();
}

Bloods::~Bloods( void )
{
    //dtor
}

void Bloods::update( void )
{

}

#define HW 80

void Bloods::draw( void )
{
    if( alive )
    {
        SDL_Rect foo = { _syncPoint->x - HW/2, _syncPoint->y - HW/2, HW, HW };
        gTextures[ TexHolder::tBLOOD ].render( foo, { 255, 255, 255, 120 }, _tile, _angle );
    }
    Actor::draw();
}

void Bloods::start( const SDL_Point& pos )
{
    Actor::start( pos );
    alive = true;
}

void Bloods::_sync_all( void )
{

}
