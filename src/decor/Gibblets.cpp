#include "decor/Gibblets.h"

#include <SDL2/SDL.h>
#include <mechanical/TexHolder.h>
#include <gert_Wrappers.h>
#include <random>

#ifdef __GERT_DEBUG__
#include <iostream>
#endif // __GERT_DEBUG__

#define LIFE_MAX 1.0
#define GIB_SPEED 20
#define DELTA_RNG 200

Gibblets::Gibblets( void )
{
    //ctor
    _lifeTime = 0;
    _tile = rand() % gTextures[ TexHolder::tGIB ].get_maxFrames();
    _xDelta = rand() % DELTA_RNG - DELTA_RNG/2;
    _yDelta = rand() % DELTA_RNG - DELTA_RNG/2;
}

Gibblets::~Gibblets( void )
{
    //dtor
}


void Gibblets::update( void )
{
    if( alive )
    {
        _lifeTime += gFrameTime;
        if( _lifeTime > LIFE_MAX )
        {
            alive = false;
        }

        _syncPoint->x += float( _xDelta )/( DELTA_RNG / 2 ) * GIB_SPEED * gFrameTime;
        _syncPoint->y += float( _yDelta )/( DELTA_RNG / 2 ) * GIB_SPEED * gFrameTime;
    }
}

#define HW 20

void Gibblets::draw( void )
{
    if( alive )
    {
        SDL_Rect foo = { _syncPoint->x - HW/2, _syncPoint->y - HW/2, HW, HW };
        SDL_Color c = color::WHITE;
        c.a = int( ( LIFE_MAX - _lifeTime )/LIFE_MAX * 254 )%255;

        gTextures[ TexHolder::tGIB ].render( foo, c, _tile, _angle );
    }
    Actor::draw();
}

void Gibblets::start( const SDL_Point& pos )
{
    Bloods::start( pos );
    _lifeTime = 0;
}
