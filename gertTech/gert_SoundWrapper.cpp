#include "gert_SoundWrapper.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <iostream>

//SOUND WRAPPING
Sound_Wrapper::Sound_Wrapper( void )
{
    _mySound = nullptr;
}

Sound_Wrapper::~Sound_Wrapper( void )
{
    close_sound();
}

void Sound_Wrapper::close_sound( void )
{
    if( _mySound != nullptr )
    {
        Mix_FreeChunk( _mySound );
        _mySound = nullptr;
    }
}

void Sound_Wrapper::load_sound( std::string fPath )
{
    close_sound();
    _mySound = Mix_LoadWAV( fPath.c_str() );
    if( _mySound == NULL )
    {
        std::cout << "Couldn't load WAV " << fPath << " \n>" << Mix_GetError() << std::endl;
    }
}

void Sound_Wrapper::set_volume( int volume )
{
    if( volume <= MIX_MAX_VOLUME )
    {
        Mix_VolumeChunk( _mySound, volume );
    }
}

bool Sound_Wrapper::play( SDL_Rect* source )
{
    if( source == NULL )
    {
        Mix_PlayChannel( -1, _mySound, 0 );
        return true;
    }
    else
    {
        SDL_Rect area = *source;
        if( ( area.x + area.w > gCamera.x && area.x < gCamera.x + gCamera.w ) &&
            ( area.y + area.h > gCamera.y && area.y < gCamera.y + gCamera.h ) )
        {
            Mix_PlayChannel( -1, _mySound, 0 );
            return true;
        }
        else
        {
            return false;
        }
    }
}
