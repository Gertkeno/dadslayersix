#ifndef TEXTURES_H
#define TEXTURES_H

#include <string>
#include <SDL2/SDL.h>

extern SDL_Renderer* gRenderer;
extern SDL_Rect gCamera;

namespace color
{
    const SDL_Color BLACK = { 0, 0, 0, 255 };

    const SDL_Color RED = { 255, 0, 0, 255 };
    const SDL_Color GREEN = { 0, 255, 0, 255 };
    const SDL_Color BLUE = { 0, 0, 255, 255 };

    const SDL_Color YELLOW = { 255, 255, 0, 255 };
    const SDL_Color CYAN = { 0, 255, 255, 255 };
    const SDL_Color MAGENTA = { 255, 0, 255, 255 };

    const SDL_Color WHITE = { 255, 255, 255, 255 };
}

class MultiFrame_Wrap
{
private:
    SDL_Texture* _myTexture;
    SDL_Rect* _frames;
    int _maxFrames;
public:
    MultiFrame_Wrap( void );
    void load_texture( std::string fPath, Uint8 segWidthNum = 0, Uint8 segHeightNum = 0, Uint16 width = 0, Uint16 height = 0, Uint8 margin = 0, Uint16 customFrameNum = 0 );
    void load_texture( std::string fPath, std::string sheetdatextension );
    void close_texture( void );

    bool render( SDL_Rect pos, SDL_Color color, int frame = 0, double a = 0.0, SDL_RendererFlip f = SDL_FLIP_NONE, SDL_Point* p = NULL );

    void enter_custom_frame( int, SDL_Rect );
    void set_blend( SDL_BlendMode blend = SDL_BLENDMODE_NONE );
    int get_maxFrames( void ) { return _maxFrames; }
    SDL_Rect get_frame_data( int frame );

    ~MultiFrame_Wrap( void );
};

#endif // TEXTURES_H
