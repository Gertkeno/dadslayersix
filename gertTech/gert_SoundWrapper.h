#ifndef GERT_SOUNDWRAPPER_H
#define GERT_SOUNDWRAPPER_H

class Mix_Chunk;
struct SDL_Rect;

#include <string>

extern SDL_Rect gCamera;

class Sound_Wrapper
{
private:
    Mix_Chunk* _mySound;
public:
    Sound_Wrapper( void );
    ~Sound_Wrapper( void );

    void close_sound( void );
    void load_sound( std::string fPath );
    bool play( SDL_Rect* source = NULL );
    void set_volume( int volume );
};

#endif // GERT_SOUNDWRAPPER_H
