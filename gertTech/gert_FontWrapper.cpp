#include "gert_FontWrapper.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include <iostream>

//FONT WRAPPER
Font_Wrapper::Font_Wrapper( void )
{
    _myFont = nullptr;
    _lastTexture = nullptr;
    _textSize = nullptr;
}

Font_Wrapper::Font_Wrapper( std::string fontPath, int fontSize )
{
    _textSize = new int( fontSize );
    _myFont = TTF_OpenFont( fontPath.c_str(), fontSize );
    if( _myFont == NULL )
    {
        std::cout << "Couldn't load font \n>" << TTF_GetError() << std::endl;
    }
    TTF_SizeText( _myFont, "Test", NULL, &t_height );
    _lastString = "NULL";
    _lastTexture = nullptr;
}

bool Font_Wrapper::render( SDL_Rect area, std::string text, SDL_Color color, bool strech )
{
    if( ( area.x + area.w > gCamera.x && area.x < gCamera.x + gCamera.w ) &&
        ( area.y + area.h > gCamera.y && area.y < gCamera.y + gCamera.h ) )
    {
        if( !strech && text.length() > 0 ) //text wrapping past edges
        {
            int wStrech = 0;// = text.length() * (*_textSize)/FONT_MULTI;
            TTF_SizeText( _myFont, text.c_str(), &wStrech, NULL );
            if( wStrech < area.w )
            {
                area.w = wStrech;
            }
            else
            {
                int wStrLimit = 0;
                for( int tempWidth = 0; tempWidth < area.w; TTF_SizeText( _myFont, text.substr( 0, wStrLimit ).c_str(), &tempWidth, NULL ) )
                {
                    wStrLimit += 2;
                }
                wStrLimit -= 1;

                if( wStrLimit < int( text.length() ) && wStrLimit > 0 )
                {
                    std::string slicedText = text.substr( wStrLimit );

                    SDL_Rect subArea = { area.x, area.y + *_textSize, area.w, area.h - *_textSize };
                    text = text.substr( 0, wStrLimit );
                    render( subArea, slicedText, color );
                }
            }

            area.h = *_textSize;
        }// STRECH TEXT WRAPPING

        area.x -= gCamera.x;
        area.y -= gCamera.y;
        if( _lastTexture != nullptr && _lastString == text );
        else
        {
            SDL_DestroyTexture( _lastTexture );
            _lastTexture = nullptr;
            _lastString = text;
            SDL_Surface *tempMsg = TTF_RenderText_Blended( _myFont, text.c_str(), { 255, 255, 255, 255 } );
            if( tempMsg == NULL )
            {
                //std::cout << "Couldn't create text \n>" << TTF_GetError() << std::endl;
            }
            else
            {
                _lastTexture = SDL_CreateTextureFromSurface( gRenderer, tempMsg );
            }
            SDL_FreeSurface( tempMsg );
        } // NOT THE LAST STRING
        if( text != "NULL" )
        {
            SDL_SetTextureAlphaMod( _lastTexture, color.a );
            SDL_SetTextureColorMod( _lastTexture, color.r, color.g, color.b );
            if( !strech ) TTF_SizeText( _myFont, _lastString.c_str(), &area.w, &area.h );
            SDL_RenderCopy( gRenderer, _lastTexture, NULL, &area );
        }
    }
    return false;
}

int Font_Wrapper::string_width( std::string ss )
{
    int w;
    TTF_SizeText( _myFont, ss.c_str(), &w, NULL );
    return w + 1;
}

Font_Wrapper::~Font_Wrapper( void )
{
    SDL_DestroyTexture( _lastTexture );
    TTF_CloseFont( _myFont );
    delete _textSize;
}
