#include "gert_CameraMath.h"

#include <cmath>
#include <gert_Collision.h>

SDL_Rect reduce_rect( SDL_Rect area, float reduction )
{
    float widthReduct = float( area.w ) * reduction;
    float heightReduct = float( area.h ) * reduction;

    SDL_Rect outRect;
    outRect.x = area.x + widthReduct / 2;
    outRect.w = area.w - widthReduct;
    outRect.y = area.y + heightReduct / 2;
    outRect.h = area.h - heightReduct;
    return outRect;
}

SDL_Rect rectAlign::left( const SDL_Rect& s, const SDL_Rect& c )
{
    SDL_Rect foo = s;
    foo.x = c.x;
    return foo;
}

SDL_Rect rectAlign::middle( const SDL_Rect& s, const SDL_Rect& c )
{
    SDL_Rect foo = s;
    foo.x = c.x + c.w/2 - s.w/2;
    return foo;
}

SDL_Rect rectAlign::right( const SDL_Rect& s, const SDL_Rect& c )
{
    SDL_Rect foo = s;
    foo.x = c.x + c.w - s.w;
    return foo;
}

double angles::get_angle( const SDL_Rect &objA, const int &pointX, const int &pointY )
{
    double amY, amX;
    amX = objA.x - gCamera.x + ( objA.w / 2 );
    amY = objA.y - gCamera.y + ( objA.h / 2 );

    double yDist = pointY - amY;
    double xDist = pointX - amX;
    // +360 ) % 360 allows for the maximum to be 360 without allowing negative numbers; while still being accurate to a degree.
    // note that this is reasonable but less percise since modulus only works on integers opposed to floats.
    double finMath = atan2( yDist, xDist ) * 180 / M_PI;
    while( finMath < 0 )
    {
        finMath += 360;
    }
    return finMath;
}

double angles::get_angle( const SDL_Rect &objA, const SDL_Rect &objB )
{
    double aoX, aoY, boX, boY;
    aoX = objA.x - gCamera.x + ( objA.w / 2 );
    aoY = objA.y - gCamera.y + ( objA.h / 2 );
    boX = objB.x - gCamera.x + ( objB.w / 2 );
    boY = objB.y - gCamera.y + ( objB.h / 2 );

    double yDist = boY - aoY;
    double xDist = boX - aoX;
    double finMath = atan2( yDist, xDist ) * 180 / M_PI;
    while( finMath < 0 )
    {
        finMath += 360;
    }
    return finMath;
}

double angles::get_angle( const Circle &objA, const int &pointX, const int &pointY )
{
    double amY, amX;
    // Circle.x and Circle.y are already the mid-point.
    amX = objA.x - gCamera.x;
    amY = objA.y - gCamera.y;

    double yDist = pointY - amY;
    double xDist = pointX - amX;

    double finMath = atan2( yDist, xDist ) * 180 / M_PI;
    while( finMath < 0 )
    {
        finMath += 360;
    }
    return finMath;
}

double angles::get_angle( const Circle &objA, const Circle &objB )
{
    double aoX, aoY, boX, boY;
    boY = objB.y - gCamera.y;
    boX = objB.x - gCamera.x;
    aoY = objA.y - gCamera.y;
    aoX = objA.x - gCamera.x;

    double distY = boY - aoY;
    double distX = boX - aoX;
    double finMath = atan2( distY, distX ) * 180 / M_PI;
    while( finMath < 0 )
    {
        finMath += 360;
    }
    return finMath;
}

double angles::get_angle( const SDL_Point& a, const SDL_Point& b )
{
    double distX = b.x - a.x;
    double distY = b.y - a.y;

    double finMath = atan2( distY, distX ) * 180 / M_PI;
    while( finMath < 0 )
    {
        finMath += 360;
    }
    return finMath;
}
