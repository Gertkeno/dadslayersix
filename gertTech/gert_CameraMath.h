#ifndef GERT_CAMERAMATH_H
#define GERT_CAMERAMATH_H

#include <SDL2/SDL.h>

extern SDL_Rect gCamera;

class Circle;

template<typename InType> // templates need to be declared not prototyped since they aren't really functions
InType add_camera( InType source, bool negative = false )
{
    if( negative )
    {
        source.x -= gCamera.x;
        source.y -= gCamera.y;
        return source;
    }
    else
    {
        source.x += gCamera.x;
        source.y += gCamera.y;
        return source;
    }
}
SDL_Rect reduce_rect( SDL_Rect, float );

namespace rectAlign
{
    SDL_Rect left( const SDL_Rect& source, const SDL_Rect& container );
    SDL_Rect middle( const SDL_Rect& source, const SDL_Rect& container );
    SDL_Rect right( const SDL_Rect& source, const SDL_Rect& container );
}

//Returning double for ease of access with corresponding render functions
namespace angles
{
    double get_angle( const SDL_Rect& source, const int& x, const int& y );
    double get_angle( const SDL_Rect& source, const SDL_Rect& obj );
    double get_angle( const Circle& source, const int& x, const int& y );
    double get_angle( const Circle& source, const Circle& obj );
    double get_angle( const SDL_Point& a, const SDL_Point& b );
}

#endif // GERT_CAMERAMATH_H
