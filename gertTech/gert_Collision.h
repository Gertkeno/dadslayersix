#ifndef COLLISION_H
#define COLLISION_H

struct SDL_Rect;
struct SDL_Point;

#include <cmath>

struct Circle
{
    float x, y, r;
    //copy to SDL_Rect
    SDL_Rect ct_Rect( void );
};

namespace collision
{

    //double distance( const float&, const float&, const float&, const float& );
    template<typename kind>
    double distance( const kind &x1, const kind &x2, const kind &y1, const kind &y2 )
    {
        kind distX = x2 - x1;
        kind distY = y2 - y1;
        return sqrt( ( distY * distY ) + ( distX * distX ) );
    }
    double distance( const SDL_Point& a, const SDL_Point& b );

    bool get_collide( const SDL_Rect& objA, const SDL_Rect& objB );
    //Circle collisions
    bool get_collide( const Circle&, const Circle& );
    bool get_collide( const Circle& a, const SDL_Rect& box );

    void get_normal_diffXY( const SDL_Point& a, const SDL_Point& b, float* diffX, float* diffY );
}

#endif // COLLISION_H
